﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.DTO
{
    public class RoundAnswersDTO
    {
        List<String> _answers;
        int _roundID;
        int _playerID;
        int _sessionID;
        int _idOpponent;

        public List<string> Answers { get => _answers; set => _answers = value; }
        public int RoundID { get => _roundID; set => _roundID = value; }
        public int PlayerID { get => _playerID; set => _playerID = value; }
        public int SessionID { get => _sessionID; set => _sessionID = value; }
        public int IdOpponent { get => _idOpponent; set => _idOpponent = value; }

        public RoundAnswersDTO()
        {
            _answers = new List<String>();
        }
    }
}
