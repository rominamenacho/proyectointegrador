﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.DTO
{
    [System.Serializable]
    public class PlayerDTO
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public List<object> SessionGameList { get; set; }
        public int Victories { get; set; }
        public WalletDTO Wallet { get; set; }
        public bool IsLookingForPlay { get; set; }
        public DateTime LoginDate { get; set; }
        public ErrorDetailsDTO ErrorDetails { get; set; }

        public PlayerDTO()
        {
            ErrorDetails = new ErrorDetailsDTO();
        }
    }
}
