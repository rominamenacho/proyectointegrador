﻿using System.Collections.Generic;

namespace Assets.Scripts.DTO
{
    public class SessionDTO
    {
        public List<RoundDTO> Rounds { get; set; }
        public bool IsInProcess { get; set; }
        public bool IsWaitingForPlayers { get; set; }
        public int Id { get; set; }
        public List<List<int>> scoreByPlayer { get; set; }

        public ErrorDetailsDTO ErrorDetails { get; set; }

        public SessionDTO()
        {
            ErrorDetails = new ErrorDetailsDTO();
        }
    }
}
