using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErrorDetailsDTO 
{
    public int StatusCode { get; set; }
    public string Message { get; set; }
    public string StackTrace { get; set; }
}
