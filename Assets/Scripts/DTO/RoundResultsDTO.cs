﻿using System.Collections.Generic;

namespace Assets.Scripts.DTO
{
    public class RoundResultsDTO
    {
        public List<TurnsDTO> Turns { get; set; }
        public string WinnerName { get; set; }
        public string OpponentName { get; set; }
        public List<string> Categories { get; set; }
        public ErrorDetailsDTO ErrorDetails { get; set; }

        public RoundResultsDTO()
        {
            ErrorDetails = new ErrorDetailsDTO();
        }
    }
}
