﻿using System.Collections.Generic;

namespace Assets.Scripts.DTO
{
    public class TurnsDTO
    {
        public List<string> Answers { get; set; }
        public int CorrectAnswersCount { get; set; }

        public bool IsFinished { get; set; }
        public int MyRoundId { get; set; }
        public int PlayerId { get; set; }

    }
}
