﻿using System.Collections.Generic;

namespace Assets.Scripts.DTO
{
    public class RoundDTO
    {
        public List<CategoryDTO> Categories { get; set; }
        public string Letter { get; set; }
        public int Id { get; set; }
        public List<TurnsDTO> Turns { get; set; }
        public int IdWinnerOfRound { get; set; }
        public ErrorDetailsDTO ErrorDetails { get; set; }
        public RoundDTO()
        {
            ErrorDetails = new ErrorDetailsDTO();
        }

    }
}
