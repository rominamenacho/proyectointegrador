﻿namespace Assets.Scripts.DTO
{
    public class AnswerDTO
    {
        string _word;
        bool _correct;

        public string Word { get; set; }
        public bool Correct { get; set; }
    }
}
