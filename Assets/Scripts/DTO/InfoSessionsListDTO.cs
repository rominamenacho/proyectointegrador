﻿using System.Collections.Generic;

namespace Assets.Scripts.DTO
{
    public class InfoSessionsListDTO
    {
        private int _idSession;
        private int _idPlayer;
        private int _idOpponent;
        private int _idRound;
        private bool _isInProcess;
        private bool _roundIsOver;
        private List<List<int>> _result = new List<List<int>>();
        private string _opponentName;
        private int _playerIndex;
        private ErrorDetailsDTO _errorDetails;

        public int IdSession { get => _idSession; set => _idSession = value; }
        public int IdPlayer { get => _idPlayer; set => _idPlayer = value; }
        public int IdOpponent { get => _idOpponent; set => _idOpponent = value; }
        public int IdRound { get => _idRound; set => _idRound = value; }
        public bool IsInProcess { get => _isInProcess; set => _isInProcess = value; }
        public bool RoundIsOver { get => _roundIsOver; set => _roundIsOver = value; }
        public List<List<int>> Result { get => _result; set => _result = value; }
        public string OpponentName { get => _opponentName; set => _opponentName = value; }
        public int PlayerIndex { get => _playerIndex; set => _playerIndex = value; }
        public ErrorDetailsDTO ErrorDetails { get => _errorDetails; set => _errorDetails = value; }

        public InfoSessionsListDTO()
        {
            ErrorDetails = new ErrorDetailsDTO();
        }
    }
}
