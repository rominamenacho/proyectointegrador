﻿using System.Collections.Generic;

namespace Assets.Scripts.DTO
{
    public class AnswersCheckedDTO
    {
        public List<AnswerDTO> Answers { get; set; }
        public int CorrectCount { get; set; }
        public int PlayerVictories { get; set; }
        public int PlayerCoins { get; set; }
        public List<ErrorDetailsDTO> ErrorDetailsDTO { get; set; }

        public AnswersCheckedDTO()
        {
            Answers = new List<AnswerDTO>();
            ErrorDetailsDTO = new List<ErrorDetailsDTO>();
        }
    }

}
