﻿using System.Collections.Generic;

namespace Assets.Scripts.DTO
{
    public class CategoryDTO
    {
        public List<string> Words { get; set; }
        public string Name { get; set; }
    }
}
