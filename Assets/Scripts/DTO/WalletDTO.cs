﻿namespace Assets.Scripts.DTO
{
    public class WalletDTO
    {
        public int Coins { get; set; }
    }
}
