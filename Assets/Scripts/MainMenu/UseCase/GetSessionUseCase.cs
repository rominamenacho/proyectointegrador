﻿using Assets.Scripts.DTO;
using Assets.Scripts.MainMenu.Service;

namespace Assets.Scripts.ModuleSession.UseCase
{
    public class GetSessionUseCase
    {

        public SessionDTO GetSessionById(int id)
        {
            SessionByIdService service = new SessionByIdService();
            return service.Execute(id);
        }

    }
}
