﻿using System;

public class CheckRewardUseCase
{
    public bool Check(DateTime lastDateInPlayer, int idPlayer)
    {
        if (lastDateInPlayer.Day < DateTime.Today.Day)
        {
            SetDailyRewardService setDailyReward = new SetDailyRewardService();
            return setDailyReward.Execute(idPlayer);
        }
        return false;
    }
}