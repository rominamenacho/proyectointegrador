﻿using Assets.Scripts.DTO;
using Assets.Scripts.MainMenu.Service;
using UnityEngine;

namespace Assets.Scripts.MainMenu.Presenter
{
    public class UpdatePlayerDataPanelUseCase
    {

        public bool Update(int idPlayer)
        {
            ObtainPlayerDataService _service = new ObtainPlayerDataService();
            PlayerDTO _playerDTO = _service.Execute(idPlayer);
            if (_playerDTO.ErrorDetails.StatusCode == 200)
            {
                UpdateDataPlayerPrefs(_playerDTO);
                return true;
            }
            return false;
        }

        private void UpdateDataPlayerPrefs(PlayerDTO _playerDTO)
        {
            PlayerPrefs.SetInt("Coins", _playerDTO.Wallet.Coins);
            PlayerPrefs.SetInt("Victories", _playerDTO.Victories);
            PlayerPrefs.SetString("LoginDate", _playerDTO.LoginDate.ToString());
        }


    }
}
