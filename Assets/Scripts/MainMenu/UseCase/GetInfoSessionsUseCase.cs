﻿using Assets.Scripts.DTO;
using Assets.Scripts.MainMenu.Service;
using System.Collections.Generic;

public class GetInfoSessionsUseCase
{

    public List<InfoSessionsListDTO> GetInfoSessions(int idPlayer)
    {
        SessionsByPlayerService service = new SessionsByPlayerService();
        return service.Execute(idPlayer);

    }


}