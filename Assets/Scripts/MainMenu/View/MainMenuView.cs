using Assets.Scripts;
using Assets.Scripts.DTO;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour, IMainMenu
{
    [SerializeField] TextMeshProUGUI playerName;
    [SerializeField] TextMeshProUGUI playerVictories;
    [SerializeField] TextMeshProUGUI playerCoins;

    [SerializeField] private GameObject SessionItemsPrefab;
    [SerializeField] private GameObject SessionList;
    [SerializeField] private GameObject DailyRewardMessage;
    [SerializeField] private Button _okButtonRewardPanel;
    [SerializeField] private Button _okButtonErrorPanel;

    [SerializeField] private Button _buttonStart;
    [SerializeField] private Button _buttonExit;
    [SerializeField] private Button _buttonUpdateSessions;

    [SerializeField] private GameObject _errorPanel;
    [SerializeField] private GameObject _errorBody;



    public event Action ButtonStartGameClicked = () => { };
    public event Action<int, int> ButtonContinueClicked = (int idSession, int idOpponent) => { };
    public event Action<int, int> ButtonViewResultsClicked = (int idRound, int idSession) => { };
    public event Action ButtonUpdateSessionsClicked = () => { };
    public event Action ButtonExitClicked = () => { };
    public event Action<int> ButtonRematchClicked = (int idOpponent) => { };
    public event Action ButtonOkRewardClicked = () => { };
    public event Action ButtonOkErrorClicked = () => { };

    UnityEvent NewNotification;


    void Start()
    {
        MainMenuPresenter _mainMenuPresenter = new MainMenuPresenter(this);
        AddListeners();
        ReceiveFromWebsocket();

        if (NewNotification == null) NewNotification = new UnityEvent();
        NewNotification.AddListener(MainThreadCall);

    }

    void Update()
    {
        SetPlayerData();
    }
    private void ReceiveFromWebsocket()
    {

        HubConnection connection;

        ConnectionHUB conHub = ConnectionHUB.GetInstance();
        connection = ConnectionHUB.connection;
        connection.On<bool>("Notify", data => NewNotification.Invoke());
    }



    private void ConnectionClosed(Exception arg)
    {
        ShowError("Hub cerrado. " + arg.Message);
    }
    public void SetPlayerData()
    {
        playerName.text = PlayerPrefs.GetString("Name");
        playerVictories.text = PlayerPrefs.GetInt("Victories").ToString();
        playerCoins.text = PlayerPrefs.GetInt("Coins").ToString();
    }

    public void ShowError(string error)
    {
        _errorPanel.SetActive(true);
        _errorBody.GetComponent<TextMeshProUGUI>().text = error;
    }
    public void CloseErrorPanel()
    {
        _errorPanel.SetActive(false);

    }
    public void Reload()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public IEnumerator RefreshOnTheMainThread()
    {
        Reload();
        yield return null;
    }

    public void MainThreadCall()
    {
        UnityMainThreadDispatcher.Instance().Enqueue(RefreshOnTheMainThread());
    }

    public void CloseRewardMessage()
    {
        DailyRewardMessage.SetActive(false);
    }
    public void CloseErrorMessage()
    {
        _errorPanel.SetActive(false);
    }

    void OnApplicationQuit()
    {
        PlayerPrefs.DeleteAll();
    }

    private void AddListeners()
    {
        _buttonStart.onClick.AddListener(() =>
        {
            ButtonStartGameClicked.Invoke();
        });
        _buttonExit.onClick.AddListener(() =>
        {
            ButtonExitClicked.Invoke();
        });
        _okButtonRewardPanel.onClick.AddListener(() =>
        {
            ButtonOkRewardClicked.Invoke();
        });
        _okButtonErrorPanel.onClick.AddListener(() =>
        {
            ButtonOkErrorClicked.Invoke();
        });
        _buttonUpdateSessions.onClick.AddListener(() =>
        {
            ButtonUpdateSessionsClicked.Invoke();
        });
        AddDynamicListeners();
    }

    public void AddDynamicListeners()
    {
        GameObject sessionList = GameObject.Find("SessionsList");
        if (sessionList.transform.childCount > 0)
        {
            foreach (Transform item in sessionList.transform)
            {
                int idSession = Int32.Parse(item.transform.Find("idSession").transform.Find("Text").GetComponent<Text>().text);
                int idRound = Int32.Parse(item.transform.Find("idRound").transform.Find("Text").GetComponent<Text>().text);
                var idOpponent = Int32.Parse(item.transform.Find("idOpponent").transform.Find("Text").GetComponent<Text>().text);

                Button buttonContinue = item.transform.GetChild(2).GetComponent<Button>();

                buttonContinue.onClick.AddListener(() =>
                {
                    ButtonContinueClicked.Invoke(idSession, idOpponent);
                });

                item.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(() =>
                {
                    ButtonViewResultsClicked.Invoke(idRound, idSession);
                });

                item.transform.GetChild(4).GetComponent<Button>().onClick.AddListener(() =>
                {
                    ButtonRematchClicked.Invoke(idOpponent);
                });

            }
        }


    }

    public void StartNewGame()
    {
        PlayerPrefs.SetInt("idSession", 0);
        PlayerPrefs.SetInt("idOpponent", 0);
        SceneManager.LoadScene("Round");
    }

    public void ContinueGame(int idSession, int idOpponent)
    {
        PlayerPrefs.SetInt("idSession", idSession);
        SceneManager.LoadScene("Round");
    }
    public void ViewResults(int idRound, int idSession)
    {
        PlayerPrefs.SetInt("idRound", idRound);
        PlayerPrefs.SetInt("idSession", idSession);
        SceneManager.LoadScene("Results");
    }
    public void ExitSession()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene("Welcome");
    }

    public void Rematch(int idOpponent)
    {
        PlayerPrefs.SetInt("idOpponent", idOpponent);
        PlayerPrefs.SetInt("idSession", 0);
        SceneManager.LoadScene("Round");
    }
    public void DrawSessionList(List<InfoSessionsListDTO> infoSessionsListDTO)
    {

        if (infoSessionsListDTO != null)
        {

            float positionY = Screen.height / 2.08f;
            foreach (var item in infoSessionsListDTO)
            {
                GameObject sessionPrefab = Instantiate(SessionItemsPrefab);

                sessionPrefab.transform.SetParent(GameObject.Find("SessionsList").transform, true);
                sessionPrefab.transform.position = new Vector3(Screen.width / 2.2f, positionY, 0);
                sessionPrefab.transform.localScale = new Vector3(0.58f, 0.58f, 0f);

                sessionPrefab.transform.Find("ResultPlayer").GetComponent<Text>().text = ShowSessionScorePlayer(item);

                sessionPrefab.transform.Find("ResultOponent").GetComponent<Text>().text = ShowSessionScoreOponent(item);

                sessionPrefab.transform.Find("OpponentName").GetComponent<TextMeshProUGUI>().text = item.OpponentName;
                PlayerPrefs.SetString("OpponentName", item.OpponentName);
                PlayerPrefs.SetInt("idOpponent", item.IdOpponent);

                sessionPrefab.transform.Find("idSession").transform.Find("Text").GetComponent<Text>().text = item.IdSession.ToString();
                sessionPrefab.transform.Find("idRound").transform.Find("Text").GetComponent<Text>().text = item.IdRound.ToString();
                sessionPrefab.transform.Find("idOpponent").transform.Find("Text").GetComponent<Text>().text = item.IdOpponent.ToString();

                positionY -= Screen.height / 13;
            }
            positionY = GameObject.Find("SessionsList").transform.position.y;
            UpdateButtons(infoSessionsListDTO);
        }


    }

    private string ShowSessionScorePlayer(InfoSessionsListDTO info)
    {
        if (info.Result[0][0] == PlayerPrefs.GetInt("PlayerId"))
        {
            return info.Result[0][1].ToString();
        }
        return info.Result[1][1].ToString();
    }

    private string ShowSessionScoreOponent(InfoSessionsListDTO info)
    {
        if (info.Result[1][0] != PlayerPrefs.GetInt("PlayerId"))
        {
            return info.Result[1][1].ToString();

        }
        return info.Result[0][1].ToString();
    }


    public void UpdateButtons(List<InfoSessionsListDTO> games)
    {
        int i = 0;
        foreach (var game in games)
        {
            GameObject sessionPrefab = GameObject.Find("SessionsList");

            if ((game.RoundIsOver && game.PlayerIndex == 0)
                || (!game.RoundIsOver && (game.PlayerIndex == 1 || game.PlayerIndex == -1)))
            {
                SetInteractableContinueRound(i, sessionPrefab, true);
            }

            if (game.IsInProcess == false)
            {
                SetInteractableContinueRound(i, sessionPrefab, false);
                SetInteractableRematch(i, sessionPrefab, true);
            }
            if (game.RoundIsOver)
            {
                SetInteractableViewAnswers(i, sessionPrefab, true);
            }
            i++;
        }
    }

    private void SetInteractableRematch(int i, GameObject sessionPrefab, bool interactable)
    {
        sessionPrefab.transform.GetChild(i).Find("Rematch").GetComponent<Button>().interactable = interactable;
    }

    private void SetInteractableViewAnswers(int i, GameObject sessionPrefab, bool interactable)
    {
        sessionPrefab.transform.GetChild(i).Find("ViewAnswers").GetComponent<Button>().interactable = interactable;
    }

    private void SetInteractableContinueRound(int i, GameObject sessionPrefab, bool interactable)
    {
        sessionPrefab.transform.GetChild(i).Find("ContinueRound").GetComponent<Button>().interactable = interactable;
    }


    public void ShowRewardMessage()
    {
        DailyRewardMessage.SetActive(true);
    }


}
