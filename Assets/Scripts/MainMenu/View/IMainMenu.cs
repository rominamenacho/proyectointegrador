using Assets.Scripts.DTO;
using System;
using System.Collections.Generic;

public interface IMainMenu
{
    public event Action ButtonExitClicked;
    event Action ButtonStartGameClicked;
    public event Action<int, int> ButtonContinueClicked;
    event Action<int, int> ButtonViewResultsClicked;
    event Action ButtonUpdateSessionsClicked;
    event Action<int> ButtonRematchClicked;
    public event Action ButtonOkRewardClicked;
    public event Action ButtonOkErrorClicked;




    void StartNewGame();
    void ContinueGame(int idSession, int idOpponent);

    void ViewResults(int idRound, int idSession);
    void UpdateButtons(List<InfoSessionsListDTO> infoSessionsListDTO);
    void SetPlayerData();
    void DrawSessionList(List<InfoSessionsListDTO> infoSessionsListDTO);
    void ExitSession();
    void Rematch(int idOpponent);
    void Reload();
    void ShowRewardMessage();
    //void Welcome();
    void AddDynamicListeners();
    void CloseRewardMessage();
    void MainThreadCall();
    void CloseErrorMessage();
    void ShowError(string v);
}
