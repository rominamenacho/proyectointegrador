﻿using Assets.Scripts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

public class SetDailyRewardService : IDisposable
{
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }

    public bool Execute(int idPlayer)
    {
        return Task.Run(() =>
         SetDailyReward(idPlayer)).GetAwaiter().GetResult();
    }
    private async Task<bool> SetDailyReward(int idPlayer)
    {
        if (idPlayer > 0)
        {
            var json = new Dictionary<string, object>();
            json.Add("Id", idPlayer);
            var jconverted = JsonConvert.SerializeObject(json);

            var queryString = new StringContent(jconverted.ToString(), Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(new Uri(ConnectionAPI.URL + "/api/Players/SetDailyReward"), queryString);
            if (response.IsSuccessStatusCode) return true;
        }
        return false;
    }
}