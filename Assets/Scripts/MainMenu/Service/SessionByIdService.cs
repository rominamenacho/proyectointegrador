﻿using Assets.Scripts.DTO;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Assets.Scripts.MainMenu.Service
{
    public class SessionByIdService : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public SessionDTO Execute(int idSession)
        {
            return Task.Run(() => GetSessionById(idSession)).GetAwaiter().GetResult();
        }

        private async Task<SessionDTO> GetSessionById(int idSession)
        {
            SessionDTO dto = new SessionDTO();
            if (!ValidateDto(idSession))
            {
                dto.ErrorDetails = CreateResponse(400, "Datos inválidos");
                return dto;
            }

            try
            {
                HttpClient httpClient = new HttpClient();
                HttpResponseMessage httpResponse = await httpClient.GetAsync(new Uri(ConnectionAPI.URL + $"/api/SessionGetById/{idSession}"));

                httpResponse.EnsureSuccessStatusCode();
                string responseBody = await httpResponse.Content.ReadAsStringAsync();
                dto = JsonConvert.DeserializeObject<SessionDTO>(responseBody);
                dto.ErrorDetails = CreateResponse(200, "Operación exitosa");
            }
            catch (HttpRequestException httpRequestException)
            {
                dto.ErrorDetails = CreateResponse(400, httpRequestException.Message);
            }
            catch (Exception e)
            {
                dto.ErrorDetails = CreateResponse(404, e.Message);
            }
            return dto;
        }


        private bool ValidateDto(int idSession)
        {
            return (idSession > 0);
        }
        private ErrorDetailsDTO CreateResponse(int statusCode, String message)
        {
            ErrorDetailsDTO errorDetail = new ErrorDetailsDTO();
            errorDetail.StatusCode = statusCode;
            errorDetail.Message = $"Detalles del error: { message}";
            return errorDetail;
        }
    }
}
