﻿using Assets.Scripts.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Assets.Scripts.MainMenu.Service
{
    public class SessionsByPlayerService : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<InfoSessionsListDTO> Execute(int idPlayer)
        {
            return Task.Run(() =>
           SessionsByPlayer(idPlayer)).GetAwaiter().GetResult();
        }

        private async Task<List<InfoSessionsListDTO>> SessionsByPlayer(int idPlayer)
        {
            List<InfoSessionsListDTO> dto = new List<InfoSessionsListDTO>();
            if (!Validate(idPlayer))
            {
                dto.Add(new InfoSessionsListDTO { ErrorDetails = CreateResponse(400, "Player id invalido") });
                return dto;
            }
            try
            {

                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(string.Format(ConnectionAPI.URL + $"/api/GetSessionsByPlayer/IdPlayer={idPlayer}"));

                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                dto = JsonConvert.DeserializeObject<List<InfoSessionsListDTO>>(responseBody);

                return dto;
            }
            catch (HttpRequestException httpRequestException)
            {
                dto.Add(new InfoSessionsListDTO { ErrorDetails = CreateResponse(400, httpRequestException.Message) });
            }
            catch (Exception e)
            {
                dto.Add(new InfoSessionsListDTO { ErrorDetails = CreateResponse(404, e.Message) });
            }
            return dto;
        }

        private bool Validate(int idPlayer)
        {
            return (idPlayer > 0);
        }

        private ErrorDetailsDTO CreateResponse(int statusCode, String message)
        {
            ErrorDetailsDTO errorDetail = new ErrorDetailsDTO();
            errorDetail.StatusCode = statusCode;
            errorDetail.Message = $"Detalles del error: { message}";
            return errorDetail;
        }

    }
}
