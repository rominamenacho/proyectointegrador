﻿using Assets.Scripts.DTO;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Assets.Scripts.MainMenu.Service
{
    public class ObtainPlayerDataService : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public PlayerDTO Execute(int id)
        {
            return Task.Run(() => UpdatePlayerAPI(id)).GetAwaiter().GetResult();
        }

        private async Task<PlayerDTO> UpdatePlayerAPI(int idPlayer)
        {
            PlayerDTO dto = new PlayerDTO();
            if (!Validate(idPlayer))
            {
                dto.ErrorDetails = CreateResponse(400, "Player id invalido");
                return dto;
            }
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(string.Format(ConnectionAPI.URL + $"/api/Players/{idPlayer}"));

                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                dto = JsonConvert.DeserializeObject<PlayerDTO>(responseBody);
                if (dto.ErrorDetails.StatusCode == 200) dto.ErrorDetails = CreateResponse(200, "Operación exitosa");
                return dto;
            }
            catch (HttpRequestException httpRequestException)
            {
                dto.ErrorDetails = CreateResponse(400, httpRequestException.Message);
            }
            catch (Exception e)
            {
                dto.ErrorDetails = CreateResponse(404, e.Message);
            }
            return dto;

        }

        private bool Validate(int idPlayer)
        {
            return (idPlayer > 0);
        }

        private ErrorDetailsDTO CreateResponse(int statusCode, String message)
        {
            ErrorDetailsDTO errorDetail = new ErrorDetailsDTO();
            errorDetail.StatusCode = statusCode;
            errorDetail.Message = $"Detalles del error: { message}";
            return errorDetail;
        }
    }
}
