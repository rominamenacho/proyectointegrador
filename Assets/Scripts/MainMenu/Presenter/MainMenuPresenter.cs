using Assets.Scripts.DTO;
using Assets.Scripts.MainMenu.Presenter;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuPresenter
{
    private IMainMenu _view;
    private List<InfoSessionsListDTO> _sessions;

    public List<InfoSessionsListDTO> Sessions { get => _sessions; set => _sessions = value; }

    public MainMenuPresenter(IMainMenu view)
    {
        _view = view;
        Subscribe();
        UpdatePlayerPanel();
        CheckDailyReward();
        UpdateSessions();
        DrawList();
        UpdatePlayerPanel();
    }


    private void CheckDailyReward()
    {
        CheckRewardUseCase checkReward = new CheckRewardUseCase();
        DateTime date = Convert.ToDateTime(PlayerPrefs.GetString("LoginDate"));

        if (checkReward.Check(date, PlayerPrefs.GetInt("PlayerId")))
        {
            _view.ShowRewardMessage();
        }

    }

    private void DrawList()
    {
        _view.DrawSessionList(Sessions);
    }

    private void Subscribe()
    {
        _view.ButtonStartGameClicked += StartNewGame;
        _view.ButtonUpdateSessionsClicked += Update;
        _view.ButtonContinueClicked += ContinueGame;
        _view.ButtonViewResultsClicked += ViewResults;
        _view.ButtonExitClicked += ExitSession;
        _view.ButtonRematchClicked += StartRematch;
        _view.ButtonOkRewardClicked += CloseRewardMessage;
        _view.ButtonOkErrorClicked += CloseErrorMessage;

    }
    private void Unsubscribe()
    {
        _view.ButtonStartGameClicked -= StartNewGame;
        _view.ButtonUpdateSessionsClicked -= Update;
        _view.ButtonContinueClicked -= ContinueGame;
        _view.ButtonViewResultsClicked -= ViewResults;
        _view.ButtonExitClicked -= ExitSession;
        _view.ButtonRematchClicked -= StartRematch;
        _view.ButtonOkErrorClicked -= CloseErrorMessage;
    }
    private void Update()
    {
        _view.Reload();
    }

    private void CloseRewardMessage()
    {
        _view.CloseRewardMessage();
    }
    private void CloseErrorMessage()
    {
        _view.CloseErrorMessage();
    }
    private void UpdatePlayerPanel()
    {
        UpdatePlayerDataPanelUseCase panelInfo = new UpdatePlayerDataPanelUseCase();

        if (!panelInfo.Update(PlayerPrefs.GetInt("PlayerId")))
        {
            _view.ShowError("No se pudo recuperar los datos del jugador.");
        }
    }

    private void UpdateSessions()
    {

        if (PlayerPrefs.GetString("UserCreated").Equals("true"))
        {
            GetInfoSessionsUseCase _updateBackInfoUseCase = new GetInfoSessionsUseCase();
            Sessions = new List<InfoSessionsListDTO>();
            Sessions = _updateBackInfoUseCase.GetInfoSessions(PlayerPrefs.GetInt("PlayerId"));
        }
        else
        {
            _view.ShowError("Usuario no creado.No es posible actualizar tus partidas.");
        }
    }

    private void StartRematch(int idOpponent)
    {
        _view.Rematch(idOpponent);
    }

    private void ContinueGame(int idSession, int idOpponent)
    {
        _view.ContinueGame(idSession, idOpponent);
    }
    private void StartNewGame()
    {
        _view.StartNewGame();
    }
    private void ViewResults(int idRound, int idSession)
    {
        _view.ViewResults(idRound, idSession);
    }
    private void ExitSession()
    {
        _view.ExitSession();
    }



}
