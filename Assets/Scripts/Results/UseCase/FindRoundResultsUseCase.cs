﻿using Assets.Scripts.DTO;
using Assets.Scripts.Results.Service;

public class FindRoundResultsUseCase
{

    public RoundResultsDTO FindResults(int idRound, int idSession, int idPlayer)
    {
        GetRoundResultsService service = new GetRoundResultsService();

        return service.Execute(idRound, idSession, idPlayer);
    }




}