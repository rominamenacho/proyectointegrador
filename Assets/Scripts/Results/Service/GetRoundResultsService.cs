﻿using Assets.Scripts.DTO;
using Newtonsoft.Json;
using System;

using System.Net.Http;
using System.Threading.Tasks;

namespace Assets.Scripts.Results.Service
{
    public class GetRoundResultsService : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public RoundResultsDTO Execute(int idRound, int idSession, int idPlayer)
        {

            return Task.Run(() => FindResultAPI(idRound, idSession, idPlayer)).GetAwaiter().GetResult();

        }

        private async Task<RoundResultsDTO> FindResultAPI(int idRound, int idSession, int idPlayer)
        {
            RoundResultsDTO dto = new RoundResultsDTO();

            if (!ValidateDto(idRound, idSession, idPlayer))
            {
                dto.ErrorDetails = CreateResponse(400, "Datos inválidos");
                return dto;
            }

            try
            {
                HttpClient client = new HttpClient();

                HttpResponseMessage response = await client.GetAsync(string.Format(ConnectionAPI.URL + $"/api/RoundResult/IdRound={idRound}&IdSession={idSession}&IdPlayer={idPlayer}"));

                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                dto = JsonConvert.DeserializeObject<RoundResultsDTO>(responseBody);
                dto.ErrorDetails = CreateResponse(200, "Operación exitosa");
            }
            catch (HttpRequestException httpRequestException)
            {

                dto.ErrorDetails = CreateResponse(400, httpRequestException.Message);
            }
            catch (Exception e)
            {
                dto.ErrorDetails = CreateResponse(404, e.Message);
            }

            return dto;
        }

        private bool ValidateDto(int idRound, int idSession, int idPlayer)
        {
            return (idRound > 0 && idSession > 0 && idPlayer > 0);
        }
        private ErrorDetailsDTO CreateResponse(int statusCode, String message)
        {
            ErrorDetailsDTO errorDetail = new ErrorDetailsDTO();
            errorDetail.StatusCode = statusCode;
            errorDetail.Message = $"Detalles del error: { message}";
            return errorDetail;
        }
    }
}
