﻿using Assets.Scripts.DTO;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SceneShowRoundResultPresenter : IDisposable
{
    private ISceneShowRoundResultView _view;
    private RoundResultsDTO _resultsDTO;

    public RoundResultsDTO ResultsDTO { get => _resultsDTO; set => _resultsDTO = value; }

    public SceneShowRoundResultPresenter(ISceneShowRoundResultView sceneShowRoundResultView)
    {
        _view = sceneShowRoundResultView;
        ResultsDTO = new RoundResultsDTO();
        Subscribe();
        EvaluateShowDrawResults();
    }

    public void EvaluateShowDrawResults()
    {

        int _round = PlayerPrefs.GetInt("idRound");
        int _sesion = PlayerPrefs.GetInt("idSession");
        int _playerId = PlayerPrefs.GetInt("PlayerId");

        if (_round > 0 && _sesion > 0 && _playerId > 0)
        {
            GetResults(_round, _sesion, _playerId);
            DrawScreen();
        }
        else
        {
            _view.ShowErrorPanel();
        }
    }

    public void DrawScreen()
    {
        DrawPlayersName();
        DrawResults();
        DrawScores();
        DrawAnswers();
        DrawCategories();
    }


    public void Dispose()
    {
        GC.SuppressFinalize(this);
        UnsubscribeEvents();
    }

    private void UnsubscribeEvents()
    {
        _view.ButtonGoToMainMenu -= LoadSceneMainMenu;
    }

    ~SceneShowRoundResultPresenter()
    {
        Dispose();
    }
    private void DrawCategories()
    {
        _view.DrawCategories(ResultsDTO.Categories);
    }

    public void DrawAnswers()
    {
        List<string> answersPlayer = ResultsDTO.Turns.Find(t => t.PlayerId == PlayerPrefs.GetInt("PlayerId")).Answers;
        List<string> answersOpponent = ResultsDTO.Turns.Find(t => t.PlayerId != PlayerPrefs.GetInt("PlayerId")).Answers;

        _view.DrawAnswers(answersPlayer, answersOpponent);
    }

    public void GetResults(int round, int sesion, int playerId)
    {
        FindRoundResultsUseCase findResults = new FindRoundResultsUseCase();
        ResultsDTO = findResults.FindResults(round, sesion, playerId);

    }

    public void DrawPlayersName()
    {
        _view.DrawPlayersName(PlayerPrefs.GetString("Name"), ResultsDTO.OpponentName);
    }
    public void DrawResults()
    {
        if (ResultsDTO.WinnerName.Equals(PlayerPrefs.GetString("Name")))
        {

            _view.DrawResults("GANASTE!");
            _view.ShowWinFeedback();
        }
        else if (ResultsDTO.WinnerName.Equals(ResultsDTO.OpponentName))
        {
            _view.DrawResults("PERDISTE!");
            _view.ShowLoseFeedback();
        }
        else
        {
            _view.DrawResults("EMPATE");
            _view.ShowTieFeedback();
        }
    }

    private void DrawScores()
    {
        int scorePlayers = ResultsDTO.Turns.Find(t => t.PlayerId == PlayerPrefs.GetInt("PlayerId")).CorrectAnswersCount;
        int scoreOpponent = ResultsDTO.Turns.Find(t => t.PlayerId != PlayerPrefs.GetInt("PlayerId")).CorrectAnswersCount; ;

        _view.DrawScores(scorePlayers.ToString(), scoreOpponent.ToString());

    }

    private void Subscribe()
    {
        _view.ButtonGoToMainMenu += LoadSceneMainMenu;
        _view.ButtonErrorClicked += CloseErrorPanel;
    }

    private void LoadSceneMainMenu()
    {
        _view.GoToMainMenu();
    }
    private void CloseErrorPanel()
    {
        _view.CloseErrorPanel();

    }
}