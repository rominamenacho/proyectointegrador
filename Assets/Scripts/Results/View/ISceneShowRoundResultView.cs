﻿using System;
using System.Collections.Generic;

public interface ISceneShowRoundResultView
{
    event Action ButtonErrorClicked;

    event Action ButtonGoToMainMenu;
    public void DrawAnswers(List<string> _answersPlayerOne, List<string> _answersPlayerTwo);
    public void DrawCategories(List<string> categories);
    public void DrawResults(string result);
    public void DrawScores(string scoreMainPlayer, string scoreOpponent);
    public void DrawPlayersName(string mainPlayerName, string opponentName);
    void GoToMainMenu();
    void ShowTieFeedback();
    void ShowLoseFeedback();
    void ShowWinFeedback();
    void CloseErrorPanel();
    void ShowErrorPanel();
}