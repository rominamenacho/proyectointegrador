using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneShowRoundResultView : MonoBehaviour, ISceneShowRoundResultView
{

    [SerializeField] private GameObject answers;
    [SerializeField] private TextMeshProUGUI _scorePlayerOne;
    [SerializeField] private TextMeshProUGUI _scorePlayerTwo;
    [SerializeField] private TextMeshProUGUI _roundResult;
    [SerializeField] private TextMeshProUGUI _playerOneName;
    [SerializeField] private TextMeshProUGUI _opponentName;
    [SerializeField] public Button BackButton;
    [SerializeField] public Button CloseErrorPanelButton;
    [SerializeField] private Animator _feedbackAnimations;
    [SerializeField] private GameObject _errorPanel;
    [SerializeField] private GameObject _errorBody;

    public event Action ButtonGoToMainMenu = () => { };
    public event Action ButtonErrorClicked = () => { };

    private SceneShowRoundResultPresenter _sceneShowRoundResultPresenter;

    void Start()
    {
        _sceneShowRoundResultPresenter = new SceneShowRoundResultPresenter(this);
        AddListeners();
    }
    private void AddListeners()
    {
        BackButton.onClick.AddListener(() =>
        {
            ButtonGoToMainMenu.Invoke();

        });
        CloseErrorPanelButton.onClick.AddListener(() =>
        {
            ButtonErrorClicked.Invoke();

        });
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void DrawAnswers(List<string> _answersPlayerOne, List<string> _answersPlayerTwo)
    {
        int answer = 0;
        foreach (Transform item in answers.transform)
        {
            item.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = _answersPlayerOne[answer].ToLower();
            item.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = _answersPlayerTwo[answer].ToLower();
            answer++;
        }
    }

    public void DrawCategories(List<string> _categories)
    {
        int category = 0;
        foreach (Transform item in answers.transform)
        {
            item.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = _categories[category].ToLower();
            category++;
        }
    }

    public void DrawPlayersName(string mainPlayerName, string opponentName)
    {
        _playerOneName.text = mainPlayerName;
        _opponentName.text = opponentName;
    }

    public void DrawResults(string result)
    {
        _roundResult.text = result;
    }

    public void DrawScores(string scoreMainPlayer, string scoreOpponent)
    {
        _scorePlayerOne.text = scoreMainPlayer;
        _scorePlayerTwo.text = scoreOpponent;
    }

    public virtual void ShowTieFeedback()
    {
        _feedbackAnimations.Play("TieAnimation");
    }

    public virtual void ShowWinFeedback()
    {
        _feedbackAnimations.Play("WinAnimation");
    }

    public virtual void ShowLoseFeedback()
    {
        _feedbackAnimations.Play("loseAnimation");
    }

    public void ShowErrorPanel()
    {
        CreateErrorPanel("No se puede recuperar la partida.");
    }

    public void CreateErrorPanel(string error)
    {
        _errorPanel.SetActive(true);
        _errorBody.GetComponent<TextMeshProUGUI>().text = error;
    }

    public void CloseErrorPanel()
    {
        _errorPanel.SetActive(false);
        GoToMainMenu();
    }


}
