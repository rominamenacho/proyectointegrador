using Assets.Scripts.DTO;
using Assets.Scripts.ModulePlayer.Presenter;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SceneStartRoundPresenterCreateNewRoundTest
{

    private ISceneStartRoundView _view;
    private SceneStartRoundPresenter presenter;

    [SetUp]
    public void SetUp()
    {
        _view = Substitute.For<ISceneStartRoundView>();
        presenter = new SceneStartRoundPresenter(_view);
        PlayerPrefs.DeleteAll();
    }

    [Test]
    public void ShouldCreateNewRoundWhenIdSessionExist()
    {
        PlayerPrefs.SetInt("idSession", 9167);

        _view.Received(1).ShowMessageCreateNewRound();
        _view.Received(0).ShowMessageCreateSession();
        _view.Received(0).ShowMessageStartRematch();

        PlayerPrefs.DeleteKey("idSession");

    }

    [Test]
    public void ShouldCreateDTOWhenCreateRoundIsSuccess()
    {
        PlayerPrefs.SetInt("idSession", 9167);
        Assert.IsNotNull(presenter.SessionDTO);
        Assert.AreEqual(200, presenter.SessionDTO.ErrorDetails.StatusCode);

        PlayerPrefs.DeleteKey("idSession");

    }


    [Test]
    public void ShouldDrawCategoriesWhenNewRoundIsSuccess()
    {
        PlayerPrefs.SetInt("idSession", 9167);

        List<string> categories = new List<string>();

        _view.Received(1).ShowMessageCreateNewRound();
        _view.ReceivedWithAnyArgs(1).DrawCategories(categories);


        PlayerPrefs.DeleteKey("idSession");

    }
    [Test]
    public void ShouldDrawLetterWhenNewRoundIsSuccess()
    {
        PlayerPrefs.SetInt("idSession", 9167);

        _view.Received(1).ShowMessageCreateNewRound();
        _view.ReceivedWithAnyArgs(1).DrawLetter("a");


        PlayerPrefs.DeleteKey("idSession");

    }

    [Test]
    public void ShouldDrawCountOfRoundWhenNewRoundIsSuccess()
    {
        PlayerPrefs.SetInt("idSession", 9167);

        _view.Received(1).ShowMessageCreateNewRound();
        _view.DrawRoundCount(presenter.SessionDTO.Rounds.Count.ToString());

        PlayerPrefs.DeleteKey("idSession");

    }

    [Test]
    public void ShouldDrawTimeLeftWhenNewRoundIsSuccess()
    {
        PlayerPrefs.SetInt("idSession", 9167);

        _view.Received(1).ShowMessageCreateNewRound();
        _view.ReceivedWithAnyArgs().StartTimer(1f);
        PlayerPrefs.DeleteKey("idSession");

    }

    [Test]
    public void ShouldFindAnswersInInputsWhenButtonCheckIsClicked()
    {
        PlayerPrefs.SetInt("idSession", 9167);
        _view.ButtonCheckGameClicked += Raise.Event<Action>();
        _view.Received(1).FindAnswersInTextInputs();

        PlayerPrefs.DeleteKey("idSession");

    }


    [Test]
    public void ShouldBlockInputsWhenButtonCheckIsClicked()
    {
        PlayerPrefs.SetInt("idSession", 9167);
        _view.ButtonCheckGameClicked += Raise.Event<Action>();
        _view.Received(1).BlockInputs();
        PlayerPrefs.DeleteKey("idSession");

    }

    [Test]
    public void ShouldStopTimerWhenButtonCheckIsClicked()
    {
        PlayerPrefs.SetInt("idSession", 9167);
        _view.ButtonCheckGameClickedCheckAnswers += Raise.Event<Action>();
        _view.Received(1).StopTimer();
        PlayerPrefs.DeleteKey("idSession");
    }

    [Test]
    public void ShouldSendAnswersToChekOnServerWhenButtonCheckIsClicked()
    {
        PlayerPrefs.SetInt("idSession", 9167);
        PlayerPrefs.SetInt("PlayerId", 6224);

        List<string> answers = new List<string>() { "", "", "", "", "" };
        _view.ButtonCheckGameClickedLoadAnswers += Raise.Event<Action<List<string>>>(answers);

        Assert.IsNotNull(presenter.AnswersChecked);
        Assert.AreEqual(5, presenter.AnswersChecked.Answers.Count);
        PlayerPrefs.DeleteKey("idSession");

    }
    [Test]
    public void ShouldShowPanelErrorIfThereAreErrorsOnCheckAnswers()
    {
        PlayerPrefs.SetInt("idSession", 9167);
        List<string> answers = new List<string>();


        _view.ButtonCheckGameClickedLoadAnswers += Raise.Event<Action<List<string>>>(answers);
        presenter.AnswersChecked.ErrorDetailsDTO.Add(new ErrorDetailsDTO { StatusCode = 400 });

        _view.ReceivedWithAnyArgs(1).ShowErrorPanel("");
        PlayerPrefs.DeleteKey("idSession");

    }
    [Test]
    public void ShouldDrawScoreWhenAnswersChecked()
    {

        PlayerPrefs.SetInt("idSession", 9167);
        PlayerPrefs.SetInt("PlayerId", 6224);
        List<string> answers = new List<string>() { "", "", "", "", "" };
        _view.ButtonCheckGameClickedLoadAnswers += Raise.Event<Action<List<string>>>(answers);
        _view.ReceivedWithAnyArgs(1).DrawScoreRound("0");

    }
    [Test]
    public void ShouldUpdatePlayerPanelWhenAnswersChecked()
    {
        PlayerPrefs.SetInt("idSession", 9167);
        PlayerPrefs.SetInt("PlayerId", 6224);

        List<string> answers = new List<string>() { "", "", "", "", "" };
        _view.ButtonCheckGameClickedLoadAnswers += Raise.Event<Action<List<string>>>(answers);

        int victories = PlayerPrefs.GetInt("Victories");
        int coins = PlayerPrefs.GetInt("Coins");

        Assert.IsNotNull(victories);
        Assert.IsNotNull(coins);
    }

    [Test]
    public void ShouldShowIconsCorrectAndWrongWhenAnswersWasChecked()
    {
        PlayerPrefs.SetInt("idSession", 9167);
        _view.ButtonCheckGameClickedLoadAnswers += Raise.Event<Action<List<string>>>(new List<string>());
        _view.ReceivedWithAnyArgs(1).FillIconsAnswers(new AnswersCheckedDTO());
        _view.ReceivedWithAnyArgs(1).ShowCorrectIconInScreen();
        _view.ReceivedWithAnyArgs(1).ShowWrongIconInScreen();
        PlayerPrefs.DeleteKey("idSession");

    }

    [Test]
    public void ShouldChangeInteractableButtonsWhenAnswersWasChecked()
    {
        PlayerPrefs.SetInt("idSession", 9167);
        _view.ButtonCheckGameClicked += Raise.Event<Action>();
        _view.Received(1).UpdateInteractablePropertyButtons();
        PlayerPrefs.DeleteKey("idSession");
    }

    [Test]
    public void ShouldLoadSceneMainMenuWhenButtonGoHomeIsClicked()
    {
        PlayerPrefs.SetInt("idSession", 9167);
        _view.ButtonGoToMainMenu += Raise.Event<Action>();
        _view.Received(1).GoToMainMenu();
        PlayerPrefs.DeleteKey("idSession");
    }

    [Test]
    public void ShouldCloseErrorPanelWhenButtonCloseErrorIsClicked()
    {
        PlayerPrefs.SetInt("idSession", 9167);
        PlayerPrefs.SetInt("PlayerId", 6224);

        _view.ButtonErrorClicked += Raise.Event<Action>();
        _view.Received(1).CloseErrorPanel();
        PlayerPrefs.DeleteKey("idSession");

    }



}
