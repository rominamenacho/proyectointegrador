using Assets.Scripts.DTO;
using Assets.Scripts.MainMenu.Service;
using NUnit.Framework;
using System.Collections.Generic;

public class SessionsByPlayerServiceTest
{
    SessionsByPlayerService service;
    List<InfoSessionsListDTO> dto;

    [SetUp]
    public void SetUp()
    {
        service = new SessionsByPlayerService();
        dto = new List<InfoSessionsListDTO>();
    }

    [TestCase(0)]
    [TestCase(-1)]
    [Test]
    public void ShouldHaveNonNegativePlayerId(int idPlayer)
    {
        dto = service.Execute(idPlayer);
        Assert.IsTrue(dto[0].ErrorDetails.StatusCode == 400);
    }
}
