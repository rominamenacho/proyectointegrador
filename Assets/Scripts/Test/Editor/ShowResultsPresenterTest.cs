using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ShowResultsPresenterTest
{
    private ISceneShowRoundResultView _view;
    private SceneShowRoundResultPresenter _presenter;

    [SetUp]
    public void SetUp()
    {
        _view = Substitute.For<ISceneShowRoundResultView>();
        _presenter = new SceneShowRoundResultPresenter(_view);

    }

    private static void SavePlayerPref()
    {
        PlayerPrefs.SetInt("PlayerId", 6224);
        PlayerPrefs.SetInt("idSession", 9167);
        PlayerPrefs.SetInt("idRound", 790);
        PlayerPrefs.SetString("Name", "TTT");
    }

    [Test]
    public void WhenGetResultsOfRoundShoudHaveAnResultsDto()
    {
        SavePlayerPref();
        _presenter.GetResults(790, 9167, 6224);
        Assert.IsTrue(_presenter.ResultsDTO != null);
    }

    [Test]
    public void WhenGetResultsOfRoundShoudHaveAListOf5categories()
    {
        SavePlayerPref();
        _presenter.GetResults(790, 9167, 6224);
        Assert.IsTrue(_presenter.ResultsDTO.Categories.Count == 5);
    }
    [Test]
    public void WhenGetResultsOfRoundShoudHaveAOpponentName()
    {
        SavePlayerPref();
        _presenter.GetResults(790, 9167, 6224);
        Assert.IsTrue(_presenter.ResultsDTO.OpponentName != null);
    }
    [Test]
    public void WhenGetResultsOfRoundShoudHaveAlistOfAnswersForEachPlayer()
    {
        SavePlayerPref();
        _presenter.GetResults(790, 9167, 6224);
        Assert.IsTrue(_presenter.ResultsDTO.Turns[0].Answers != null);
        Assert.IsTrue(_presenter.ResultsDTO.Turns[1].Answers != null);
    }

    [Test]
    public void WhenGetResultsOfRoundShoudHaveAWinnerName()
    {
        SavePlayerPref();
        _presenter.GetResults(790, 9167, 6224);
        Assert.IsTrue(_presenter.ResultsDTO.WinnerName != null);
    }

    [Test]
    public void WhenGetResultsOfRoundShoudDrawplayersName()
    {
        SavePlayerPref();
        _presenter.GetResults(790, 9167, 6224);
        _view.Received(1).DrawPlayersName(PlayerPrefs.GetString("Name"), _presenter.ResultsDTO.OpponentName);
    }

    [Test]
    public void WhenGetResultsOfRoundShouldDrawResults()
    {
        SavePlayerPref();
        _presenter.GetResults(790, 9167, 6224);
        _view.Received(1).DrawResults("EMPATE");

    }
    [Test]
    public void WhenGetResultsOfRoundShouldDrawScores()
    {
        SavePlayerPref();
        _presenter.GetResults(790, 9167, 6224);
        _view.Received(1).DrawScores("0", "0");
    }
    [Test]
    public void WhenGetResultsOfRoundShouldDrawAnswers()
    {
        SavePlayerPref();
        List<string> player = _presenter.ResultsDTO.Turns[0].Answers;
        List<string> opponent = _presenter.ResultsDTO.Turns[1].Answers;
        _presenter.GetResults(790, 9167, 6224);
        _view.Received(1).DrawAnswers(player, opponent);
    }
    [Test]
    public void WhenGetResultsOfRoundShouldDrawCategories()
    {
        SavePlayerPref();
        List<string> cateogories = _presenter.ResultsDTO.Categories;
        _presenter.GetResults(790, 9167, 6224);
        _view.Received(1).DrawCategories(cateogories);
    }

    [Test]
    public void WhenEventIsRaiseGoMainMenuScene()
    {
        SavePlayerPref();
        _view.ButtonGoToMainMenu += Raise.Event<Action>();
        _view.Received(1).GoToMainMenu();
    }

    [Test]
    public void WhenMatchIsInvalidShouldShowErrorPanel()
    {
        PlayerPrefs.SetInt("PlayerId", 0);
        PlayerPrefs.SetInt("idSession", 0);
        PlayerPrefs.SetInt("idRound", 0);
        _view.Received(1).ShowErrorPanel();
    }

    [Test]
    public void WhenEventCloseErrorPanelIsRaiseShouldClosePanel()
    {
        _view.ButtonErrorClicked += Raise.Event<Action>();
        _view.Received(1).CloseErrorPanel();
    }

    [Test]
    public void WhenGetResultsOfRoundShouldShowAnimation()
    {
        SavePlayerPref();
        _view.Received(1).ShowTieFeedback();

    }


}
