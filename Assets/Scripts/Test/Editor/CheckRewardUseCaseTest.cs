using NUnit.Framework;
using System;

public class CheckRewardUseCaseTest
{
    CheckRewardUseCase _useCase;

    [SetUp]
    public void SetUp()
    {
        _useCase = new CheckRewardUseCase();
    }

    [Test]
    public void ShoudntGivenRewardWhenDateIsAfterToday()
    {
        bool response = _useCase.Check(DateTime.Today.AddDays(1), 2567);
        Assert.IsFalse(response);
    }
    [Test]
    public void ShoudntGivenRewardWhenDateIsToday()
    {
        bool response = _useCase.Check(DateTime.Today, 2567);
        Assert.IsFalse(response);
    }

    [Test]
    public void ShoudGivenRewardWhenDateIsBeforeToday()
    {
        bool response = _useCase.Check(DateTime.Today.AddDays(-2), 2567);
        Assert.IsTrue(response);
    }

}
