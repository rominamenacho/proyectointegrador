using NUnit.Framework;

public class SetDailyRewardServiceTest
{
    SetDailyRewardService service;

    [SetUp]
    public void SetUp()
    {
        service = new SetDailyRewardService();
    }
    [Test]
    public void ShouldReturnTrueWhenRewardIsGiven()
    {
        bool response = service.Execute(2567);
        Assert.IsTrue(response);
    }

    [Test]
    public void ShouldReturnFalseWhenIdPlayerIsZero()
    {
        bool response = service.Execute(0);
        WhenAssertIsFalse(response);
    }

    [Test]
    public void ShouldReturnFalseWhenIdPlayerIsNegativeInteger()
    {
        bool response = service.Execute(-1);
        WhenAssertIsFalse(response);
    }

    private static void WhenAssertIsFalse(bool response)
    {
        Assert.IsFalse(response);
    }
}
