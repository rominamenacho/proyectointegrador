using Assets.Scripts.ModulePlayer.Presenter;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

public class SceneStartRoundPresenterStartRematchTest
{

    private ISceneStartRoundView _view;
    private SceneStartRoundPresenter presenter;

    [SetUp]
    public void SetUp()
    {
        _view = Substitute.For<ISceneStartRoundView>();
        presenter = new SceneStartRoundPresenter(_view);
        PlayerPrefs.DeleteAll();
    }
    [Test]
    public void ShouldStartRematchWhenIdOpponentExist()
    {

        PlayerPrefs.SetInt("idOpponent", 2567);
        PlayerPrefs.SetInt("PlayerId", 6224);
        PlayerPrefs.DeleteKey("idSession");


        _view.Received(1).ShowMessageStartRematch();
        _view.Received(0).ShowMessageCreateNewRound();
        _view.Received(0).ShowMessageCreateSession();

    }
}
