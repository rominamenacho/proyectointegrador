using Assets.Scripts.MainMenu.Presenter;
using NUnit.Framework;
using UnityEngine;

public class UpdatePlayerDataPanelUseCaseTest
{
    UpdatePlayerDataPanelUseCase _useCase;

    [SetUp]
    public void SetUp()
    {
        _useCase = new UpdatePlayerDataPanelUseCase();
    }

    [TestCase(-1)]
    [TestCase(0)]
    [TestCase(1)]
    [Test]
    public void ShouldBeAnIdPlayerNonNegative(int idPlayer)
    {
        bool response = _useCase.Update(idPlayer);
        Assert.IsFalse(response);
    }
    [Test]
    public void ShouldBeAnIdPlayerValid()
    {
        bool response = _useCase.Update(2567);
        Assert.IsTrue(response);
    }

    [Test]
    public void ShouldUpdatePlayerPrefsWhenPlayerIsFinded()
    {
        //given
        int _coinsBefore = PlayerPrefs.GetInt("Coins");
        int _victoriesBefore = PlayerPrefs.GetInt("Victories");
        string _loginDate = PlayerPrefs.GetString("LoginDate");

        //then
        _useCase.Update(6224);

        //when
        Assert.AreNotEqual(PlayerPrefs.GetInt("Coins"), _coinsBefore);
        Assert.AreNotEqual(PlayerPrefs.GetInt("Victories"), _victoriesBefore);
        Assert.AreNotEqual(PlayerPrefs.GetInt("LoginDate"), _loginDate);

    }


}
