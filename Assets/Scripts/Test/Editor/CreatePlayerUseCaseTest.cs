using Assets.Scripts.DTO;
using NUnit.Framework;

public class CreatePlayerUseCaseTest
{
    CreatePlayerUseCase useCase;
    PlayerDTO _playerDto;

    [SetUp]
    public void SetUp()
    {
        useCase = new CreatePlayerUseCase();
    }



    [Test]
    public void ShouldRecievedAnIdPlayerAfterCreated()
    {
        ThenPlayerIsCreated();
        Assert.IsTrue(_playerDto.Id > 0);
    }

    [Test]
    public void ShouldRecievedPlayerNameAfterCreated()
    {
        ThenPlayerIsCreated();
        Assert.AreEqual("ANA", _playerDto.Name);
    }

    [Test]
    public void ShouldRecievedPlayerEmailAfterCreated()
    {
        ThenPlayerIsCreated();
        Assert.AreEqual("ana@gmail.com", _playerDto.Email);
    }

    [Test]
    public void ShouldRecievedLoginDateAfterCreated()
    {
        ThenPlayerIsCreated();
        Assert.IsNotNull(_playerDto.LoginDate);
    }

    private void ThenPlayerIsCreated()
    {
        _playerDto = useCase.Create("ANA", "ana@gmail.com");
    }
}
