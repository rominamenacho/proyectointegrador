using Assets.Scripts;
using Microsoft.AspNetCore.SignalR.Client;
using NUnit.Framework;
using UnityEngine;

public class ConnectToHubUseCaseTest
{

    [Test]
    public void ShuldReturnMessageWhenConnectToHub()
    {
        HubConnection connection;

        ConnectionHUB conHub = ConnectionHUB.GetInstance();
        connection = ConnectionHUB.connection;
        var message = connection.On<string>("Hello", (value) => Debug.Log(value));
        Assert.IsTrue(message != null);
    }

}
