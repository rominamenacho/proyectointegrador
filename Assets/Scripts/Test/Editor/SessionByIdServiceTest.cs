using Assets.Scripts.DTO;
using Assets.Scripts.MainMenu.Service;
using NUnit.Framework;

public class SessionByIdServiceTest
{
    SessionByIdService service;
    SessionDTO dto;

    [SetUp]
    public void SetUp()
    {
        service = new SessionByIdService();
        dto = new SessionDTO();
    }

    [TestCase(0)]
    [TestCase(-1)]
    [Test]
    public void ShouldHaveNonNegativeSessionId(int idSession)
    {
        dto = service.Execute(idSession);
        Assert.IsTrue(dto.ErrorDetails.StatusCode == 400);
    }

    [Test]
    public void ShouldRecievedAnIdSessionValid()
    {
        dto = service.Execute(9167);
        Assert.IsTrue(dto.Id > 0);
    }
    [Test]
    public void ShouldRecievedARoundListWithOneRoundAtLeast()
    {
        dto = service.Execute(9167);
        Assert.IsTrue(dto.Rounds.Count > 0);
    }
}
