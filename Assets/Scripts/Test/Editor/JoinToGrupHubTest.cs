using Assets.Scripts.ModulePlayer.Presenter;
using NUnit.Framework;

public class JoinToGrupHubTest
{
    JointoHubService service;

    [SetUp]
    public void Setup()
    {
        service = new JointoHubService();
    }

    [TestCase("", "")]
    [TestCase("123", "")]
    [TestCase("", "123")]
    [Test]
    public void ShouldReturnFalseIfAtLeastOneParameterIsEmpty(string param1, string param2)
    {
        bool result = service.Execute(param1, param2);
        Assert.IsFalse(result);
    }


    [TestCase("aa", "aa")]
    [TestCase("bc", "aabc")]
    [TestCase("CC!", "CC!")]
    [Test]
    public void ShouldReturnFalseIfAtLeastOneParameterIsNotANumber(string param1, string param2)
    {
        bool result = service.Execute(param1, param2);

        Assert.IsFalse(result);
    }

    [Test]
    public void ShouldReturnTrueIfConnectionIsSuccess()
    {
        bool result = service.Execute("1234", "364");

        Assert.IsTrue(result);
    }

}
