using Assets.Scripts.DTO;
using NUnit.Framework;

public class DoMatchUseCaseTest
{

    DoMatchUseCase _matchUC;

    SessionDTO dto;

    [SetUp]
    public void SetUp()
    {
        _matchUC = new DoMatchUseCase();
        dto = new SessionDTO();
    }

    [TestCase(0)]
    [TestCase(-1)]
    [Test]
    public void ShouldSendAnNonNegativeIntegerAsPlayerId(int PlayerId)
    {
        dto = _matchUC.Match(PlayerId);
        Assert.AreEqual(400, dto.ErrorDetails.StatusCode);
    }

    [Test]
    public void ShouldRecievedaSessionDtoAfterDoMatch()
    {
        WhenDoMatch();
        Assert.IsNotNull(dto);
    }


    [Test]
    public void ShouldRecievedAtLeastOneRoundAfterDoMatch()
    {
        WhenDoMatch();
        Assert.AreEqual(true, dto.Rounds.Count > 0);
    }

    [Test]
    public void ShouldBeASessionInProcessAfterDoMatch()
    {
        WhenDoMatch();
        Assert.AreEqual(true, dto.IsInProcess);
    }

    [Test]
    public void ShouldRecievedAnSessionIdAfterDoMatch()
    {
        WhenDoMatch();
        Assert.AreEqual(true, dto.Id > 0);
    }

    [Test]
    public void ShouldRecievedAnSStatusCode200AfterDoMatch()
    {
        WhenDoMatch();
        Assert.AreEqual(200, dto.ErrorDetails.StatusCode);
    }

    private void WhenDoMatch()
    {
        dto = _matchUC.Match(6224);
    }

}
