using Assets.Scripts.ModulePlayer.Presenter;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

public class SceneStartRoundPresenterCreateSessionTest
{
    private ISceneStartRoundView _view;
    private SceneStartRoundPresenter presenter;


    [SetUp]
    public void SetUp()
    {
        _view = Substitute.For<ISceneStartRoundView>();
        presenter = new SceneStartRoundPresenter(_view);
        PlayerPrefs.DeleteAll();
    }


    [Test]
    public void ShouldCreateSessionWhenIdPlayerIsValid()
    {
        PlayerPrefs.SetInt("PlayerId", 6224);
        PlayerPrefs.SetInt("idOpponent", 0);
        PlayerPrefs.SetInt("idSession", 0);



        _view.Received(1).ShowMessageCreateSession();
        _view.Received(0).ShowMessageCreateNewRound();
        _view.Received(0).ShowMessageStartRematch();

        PlayerPrefs.DeleteKey("idSession");
        PlayerPrefs.DeleteKey("idOpponent");
        PlayerPrefs.DeleteKey("PlayerId");
    }

}
