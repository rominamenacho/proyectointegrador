using Assets.Scripts.DTO;
using NUnit.Framework;

public class CreateRoundServiceTest
{
    CreateRoundService service;
    SessionDTO sessionDto;

    [SetUp]
    public void Setup()
    {
        service = new CreateRoundService();
        sessionDto = new SessionDTO();


    }


    [Test]
    public void ShouldntBeInvalidSessionId()
    {
        RoundDTO roundDto = new RoundDTO();
        roundDto = service.Execute(sessionDto);


        Assert.AreEqual(400, roundDto.ErrorDetails.StatusCode);
    }

    [Test]
    public void ShouldHaveValidIds()
    {
        RoundDTO roundDto = new RoundDTO();
        roundDto = service.Execute(CreateSessionDto());

        Assert.AreEqual(200, roundDto.ErrorDetails.StatusCode);
    }

    [Test]
    public void ShouldReturnNewRound()
    {
        RoundDTO roundDto = new RoundDTO();
        roundDto = service.Execute(CreateSessionDto());

        Assert.IsTrue(roundDto.Id > 0);
    }
    private SessionDTO CreateSessionDto()
    {
        return new SessionDTO()
        {
            Rounds = null,
            IsInProcess = true,
            IsWaitingForPlayers = true,
            Id = 9167,
            scoreByPlayer = null,
        };
    }
}
