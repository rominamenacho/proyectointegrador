using NSubstitute;
using NUnit.Framework;
using System;
using UnityEngine;

public class MainMenuPresenterTest
{
    private IMainMenu _view;
    private MainMenuPresenter _presenter;

    [SetUp]
    public void SetUp()
    {
        _view = Substitute.For<IMainMenu>();
        _presenter = new MainMenuPresenter(_view);
    }

    [Test]
    public void SouldTrowPanelErrorWhenCantUpdatePlayer()
    {
        PlayerPrefs.SetInt("PlayerId", 1);
        _view.Received().ShowError("No se puedo recuperar los datos del jugador.");
    }



    [Test]
    public void ShouldShowRewardMessageWhenCheckRewardIsTrue()
    {
        PlayerPrefs.SetInt("PlayerId", 6224);
        _view.Received(1).ShowRewardMessage();
    }
    [Test]
    public void ShouldntShowRewardMessageWhenCheckRewardIsFalse()
    {
        PlayerPrefs.SetInt("PlayerId", 6224);
        _view.Received(0).ShowRewardMessage();
    }
    [Test]
    public void ShouldntUpdateSessionListWhenPlayerCreatedIsFalse()
    {
        PlayerPrefs.SetString("UserCreated", "false");
        _view.Received(1).ShowError("Usuario no creado.No es posible actualizar tus partidas.");

    }
    public void ShouldUpdateSessionListWhenPlayerCreatedIsTrue()
    {
        PlayerPrefs.SetString("UserCreated", "true");
        _view.Received(0).ShowError("Usuario no creado.No es posible actualizar tus partidas.");

    }
    [Test]
    public void ShouldDrawSessionListWhenIsNotEmpty()
    {
        PlayerPrefs.SetString("UserCreated", "true");
        PlayerPrefs.SetInt("PlayerId", 6224);
        _view.Received(1).DrawSessionList(_presenter.Sessions);
    }

    [Test]
    public void ShouldCloseErrorPanelWhenRaiseEvent()
    {
        _view.ButtonOkErrorClicked += Raise.Event<Action>();
        _view.Received(1).CloseErrorMessage();
    }
    [Test]
    public void ShouldCloseRewardPanelWhenRaiseEvent()
    {
        _view.ButtonOkRewardClicked += Raise.Event<Action>();
        _view.Received(1).CloseRewardMessage();
    }
    [Test]
    public void ShouldLoadSceneRematchWhenRaiseEvent()
    {
        _view.ButtonRematchClicked += Raise.Event<Action<int>>(6224);
        _view.Received(1).Rematch(6224);
    }
    [Test]
    public void ShouldLogOutWhenRaiseEvent()
    {
        _view.ButtonExitClicked += Raise.Event<Action>();
        _view.Received(1).ExitSession();
    }
    [Test]
    public void ShouldViewResultsWhenRaiseEvent()
    {
        _view.ButtonViewResultsClicked += Raise.Event<Action<int, int>>(6224, 444);
        _view.Received(1).ViewResults(6224, 444);
    }

    [Test]
    public void ShouldGoToRoundSceneWhenRaisedEvent()
    {
        _view.ButtonContinueClicked += Raise.Event<Action<int, int>>(6224, 444);
        _view.Received(1).ContinueGame(6224, 444);
    }
    [Test]
    public void ShouldReloadSceneWhenRaisedEvent()
    {
        _view.ButtonUpdateSessionsClicked += Raise.Event<Action>();
        _view.Received(1).Reload();
    }

    [Test]
    public void ShouldGoToRoundSceneForNewGameWhenRaisedEvent()
    {
        _view.ButtonStartGameClicked += Raise.Event<Action>();
        _view.Received(1).StartNewGame();
    }


}
