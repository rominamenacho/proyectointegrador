using Assets.Scripts.DTO;
using Assets.Scripts.Round.Service;
using NUnit.Framework;

public class MatchServiceTest
{
    MatchService service;
    SessionDTO sessionDto;

    [SetUp]
    public void Setup()
    {
        service = new MatchService();
        sessionDto = new SessionDTO();


    }


    [TestCase(0)]
    [TestCase(-1)]
    [Test]
    public void ShouldntBeInvalidSessionId(int id)
    {
        sessionDto = service.Execute(id);
        Assert.AreEqual(400, sessionDto.ErrorDetails.StatusCode);
    }


    [TestCase(1)]
    [Test]
    public void ShouldReturnErrorWhenIdIsNotFound(int id)
    {
        sessionDto = service.Execute(id);
        Assert.AreEqual(404, sessionDto.ErrorDetails.StatusCode);
    }


}
