using Assets.Scripts.Round.Service;
using NUnit.Framework;

public class SaveTimeLeftServiceTest
{
    SaveTimeLeftService service;

    [SetUp]
    public void Setup()
    {
        service = new SaveTimeLeftService();

    }

    [TestCase(-4, -3, -2)]
    [TestCase(0, 0, 0)]
    [TestCase(-10, 0, 1)]
    [TestCase(0, 0, -21)]
    [TestCase(1, -20, 0)]

    [Test]
    public void ShouldHaveNonNegativeIds(int idRound, int idSession, int idPlayer)
    {
        bool isValid = service.Execute(idRound, idSession, idPlayer, 0);

        Assert.IsFalse(isValid);
    }
    [TestCase(1, 1, 1)]
    [Test]
    public void ShouldHavePositiveIds(int idRound, int idSession, int idPlayer)
    {
        bool isValid = service.Execute(idRound, idSession, idPlayer, 0);

        Assert.IsTrue(isValid);
    }


}
