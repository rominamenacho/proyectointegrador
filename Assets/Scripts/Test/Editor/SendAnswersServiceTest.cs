using Assets.Scripts.DTO;
using Assets.Scripts.Round.Service;
using NUnit.Framework;
using System.Collections.Generic;

public class SendAnswersServiceTest
{
    SendAnswersService service;
    AnswersCheckedDTO answersCheckedDTO;
    RoundAnswersDTO dto;
    [SetUp]
    public void SetUp()
    {
        CreateDto();

        service = new SendAnswersService();
        answersCheckedDTO = new AnswersCheckedDTO();
    }



    private void CreateDto()
    {
        dto = new RoundAnswersDTO();

        dto.Answers = new List<string>();
        dto.RoundID = 123;
        dto.PlayerID = 323;
        dto.SessionID = 123;
        dto.IdOpponent = 1;
    }

    [TestCase(-123)]
    [TestCase(0)]
    [Test]
    public void ShouldHaveSessionId(int sessionId)
    {
        dto.SessionID = sessionId;
        ThenExecuteService();
        WhenErrorOccurs();

    }

    [TestCase(-123)]
    [TestCase(0)]
    [Test]
    public void ShouldHaveRoundId(int roundId)
    {
        dto.RoundID = roundId;
        ThenExecuteService();
        WhenErrorOccurs();
    }

    [TestCase(-123)]
    [TestCase(0)]
    [Test]
    public void ShouldHavePlayerId(int playerId)
    {
        dto.RoundID = playerId;
        ThenExecuteService();
        WhenErrorOccurs();

    }
    [Test]
    public void ShouldReturnErrorWhenNotFoundSession()
    {

        ThenExecuteService();
        WhenErrorOccurs();
    }


    [TestCase(9167, 6224, 790, -1)]
    [Test]
    public void ShouldHaveReturnValidAnswerListOnSuccessCase(int sessionId, int playerId, int roundId, int opponentId)
    {
        dto.SessionID = sessionId;
        dto.PlayerID = playerId;
        dto.RoundID = roundId;
        dto.IdOpponent = opponentId;
        ThenExecuteService();
        Assert.AreEqual(5, answersCheckedDTO.Answers.Count);


    }

    [TestCase(9167, 6224, 790, -1)]
    [Test]
    public void ShouldReturnAnswersNonNegativeValueCorrectCountOnSuccessCase(int sessionId, int playerId, int roundId, int opponentId)
    {
        dto.SessionID = sessionId;
        dto.PlayerID = playerId;
        dto.RoundID = roundId;
        dto.IdOpponent = opponentId;
        ThenExecuteService();
        Assert.AreEqual(0, answersCheckedDTO.CorrectCount);


    }
    [TestCase(9167, 6224, 790, -1)]
    [Test]
    public void ShouldReturnNonNegativeValueCoinsOnSuccessCase(int sessionId, int playerId, int roundId, int opponentId)
    {
        dto.SessionID = sessionId;
        dto.PlayerID = playerId;
        dto.RoundID = roundId;
        dto.IdOpponent = opponentId;
        ThenExecuteService();
        Assert.AreEqual(0, answersCheckedDTO.CorrectCount);


    }
    [TestCase(9167, 6224, 790, -1)]
    [Test]
    public void ShouldReturnNonNegativeValueVictoriesOnSuccessCase(int sessionId, int playerId, int roundId, int opponentId)
    {
        dto.SessionID = sessionId;
        dto.PlayerID = playerId;
        dto.RoundID = roundId;
        dto.IdOpponent = opponentId;
        ThenExecuteService();
        Assert.AreEqual(0, answersCheckedDTO.CorrectCount);


    }


    [TestCase(9167, 6224, 790, -1)]
    [Test]
    public void ShouldReturnStatusCode200OnSuccessCase(int sessionId, int playerId, int roundId, int opponentId)
    {
        dto.SessionID = sessionId;
        dto.PlayerID = playerId;
        dto.RoundID = roundId;
        dto.IdOpponent = opponentId;
        ThenExecuteService();

        Assert.AreEqual(200, answersCheckedDTO.ErrorDetailsDTO[0].StatusCode);

    }



    private void WhenErrorOccurs()
    {
        Assert.AreEqual(400, answersCheckedDTO.ErrorDetailsDTO[0].StatusCode);
    }
    private void ThenExecuteService()
    {
        answersCheckedDTO = service.Execute(dto);
    }
}


