using Assets.Scripts.DTO;
using Assets.Scripts.Round.Service;
using NUnit.Framework;

public class RematchServiceTest
{
    RematchService _service;

    [SetUp]
    public void SetUp()
    {
        _service = new RematchService();
    }


    [TestCase(0, 0)]
    [TestCase(-1, -1)]
    [Test]
    public void IdsShoudBeNonNegativeIntegers(int mainPlayerId, int opponentPlayerId)
    {
        SessionDTO sessionDTO = _service.Execute(mainPlayerId, opponentPlayerId);
        Assert.AreEqual(400, sessionDTO.ErrorDetails.StatusCode);
    }

    [TestCase(6224, 8156)]
    [Test]
    public void ShoudReturnSuccessWhenPlayerExists(int mainPlayerId, int opponentPlayerId)
    {
        SessionDTO sessionDTO = _service.Execute(mainPlayerId, opponentPlayerId);
        Assert.AreEqual(200, sessionDTO.ErrorDetails.StatusCode);
    }

    [TestCase(1, 1)]
    [Test]
    public void ShoudReturnErrorWhenPlayerDoesntExists(int mainPlayerId, int opponentPlayerId)
    {
        SessionDTO sessionDTO = _service.Execute(mainPlayerId, opponentPlayerId);
        Assert.AreEqual(404, sessionDTO.ErrorDetails.StatusCode);
    }
}
