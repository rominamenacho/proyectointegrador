using Assets.Scripts.DTO;
using Assets.Scripts.Results.Service;
using NUnit.Framework;

public class GetRoundResultsServiceTest
{

    GetRoundResultsService service;
    RoundResultsDTO dto;

    [SetUp]
    public void SetUp()
    {
        service = new GetRoundResultsService();
        dto = new RoundResultsDTO();


    }

    [TestCase(0, 0, 0)]
    [TestCase(0, 1, 0)]
    [TestCase(0, 0, 1)]
    [TestCase(1, -20, 0)]
    [Test]
    public void ShouldHaveNonNegativeIds(int idRound, int idSession, int idPlayer)
    {
        dto = service.Execute(idRound, idSession, idPlayer);

        Assert.IsTrue(dto.ErrorDetails.StatusCode == 400);

    }


}
