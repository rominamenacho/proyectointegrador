using NSubstitute;
using NUnit.Framework;
using System;

public class LoginPresenterTest
{
    private ILoginView _view;
    private LoginPresenter _presenter;

    [SetUp]
    public void SetUp()
    {
        _view = Substitute.For<ILoginView>();
        _presenter = new LoginPresenter(_view);
    }


    [Test]
    public void PlayerNameShouldHave4LettersAsMinimum()
    {
        bool isValid = _presenter.Validate("asd", "ana@gmail.com");
        Assert.IsFalse(isValid);
    }

    [Test]
    public void PlayerNameShouldntHaveIntegers()
    {
        bool isValid = _presenter.Validate("12a34", "ana@gmail.com");
        Assert.IsFalse(isValid);
    }

    [Test]
    public void PlayerNameShouldntBeEmptyOrNull()
    {
        bool isValid = _presenter.Validate(" ", "ana@gmail.com");
        Assert.IsFalse(isValid);
    }

    [TestCase("maria", "maria@ lalala.com")]
    [TestCase("maria", "")]
    [Test]
    public void PlayerEmailShouldntBeNullOrWithSpace(string name, string email)
    {
        bool isValid = _presenter.Validate(name, email);
        Assert.IsFalse(isValid);
    }

    [Test]
    public void PlayerEmailShouldHaveArroba()
    {
        bool isValid = _presenter.Validate("maria", "maria@lalala.com");
        Assert.IsTrue(isValid);
    }

    [Test]
    public void ShowErrorPanelWhenLoginIsNotValid()
    {
        _presenter.LoginToGame("", "");
        _view.Received(1).ShowErrorPanel("Datos invalidos");
    }

    [Test]
    public void NavegateToMainMenuWhenLoginIsValid()
    {
        _presenter.LoginToGame("Maria", "maria@gmail.com");
        _view.Received(1).LoadMainMenuScene();
    }

    [Test]
    public void ShouldCloseErrorPanelWhenRaiseEvent()
    {
        _view.ButtonCloseClicked += Raise.Event<Action>();
        _view.Received(1).CloseErrorComponent();
    }
}
