using Assets.Scripts.Round.Service;
using NUnit.Framework;

public class ObtainTimeServiceTest
{
    ObtainTimeService service;


    [SetUp]
    public void SetUp()
    {
        service = new ObtainTimeService();
    }





    [TestCase(0, 0, 0)]
    [TestCase(-1, -1, -1)]
    [Test]
    public void ShouldReturnZeroIfIdsAreInvalid(int idRound, int idSession, int idPlayer)
    {
        float timeReturned = service.Execute(idRound, idSession, idPlayer);
        Assert.AreEqual(0, timeReturned);
    }

    [TestCase(1, 1, 1)]
    [Test]
    public void ShouldReturnZeroIfDoesntExistRoundSessionOrPlayer(int idRound, int idSession, int idPlayer)
    {
        float timeReturned = service.Execute(idRound, idSession, idPlayer);
        Assert.AreEqual(0, timeReturned);
    }
}
