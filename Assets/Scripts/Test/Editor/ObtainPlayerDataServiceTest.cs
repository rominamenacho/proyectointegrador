using Assets.Scripts.DTO;
using Assets.Scripts.MainMenu.Service;
using NUnit.Framework;

public class ObtainPlayerDataServiceTest
{
    ObtainPlayerDataService service;
    PlayerDTO dto;

    [SetUp]
    public void SetUp()
    {
        service = new ObtainPlayerDataService();
        dto = new PlayerDTO();
    }

    [TestCase(0)]
    [TestCase(-1)]
    [Test]
    public void ShouldHaveNonNegativePlayerId(int idPlayer)
    {
        dto = service.Execute(idPlayer);
        Assert.IsTrue(dto.ErrorDetails.StatusCode == 400);
    }

    [Test]
    public void ShouldRecievedAnIdPlayerValid()
    {
        dto = service.Execute(6224);
        Assert.IsTrue(dto.Id > 0);
    }

    [Test]
    public void ShouldRecievedPlayerNameValid()
    {
        dto = service.Execute(6224);
        Assert.IsTrue(dto.Name.Length > 0);
    }



    [Test]
    public void ShouldRecievedLoginDateValid()
    {
        dto = service.Execute(6224);
        Assert.IsNotNull(dto.LoginDate);
    }

}
