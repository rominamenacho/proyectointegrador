﻿using Assets.Scripts.Round.Service;

namespace Assets.Scripts.Round.UseCase
{
    class SaveTimeLeftUseCase
    {
        internal bool SaveTime(int playerId, int idRound, int idSession, float seconds)
        {
            SaveTimeLeftService service = new SaveTimeLeftService();
            return service.Execute(playerId, idRound, idSession, seconds);
        }
    }
}
