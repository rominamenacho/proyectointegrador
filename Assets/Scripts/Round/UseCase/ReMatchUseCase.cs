﻿using Assets.Scripts.DTO;
using Assets.Scripts.Round.Service;

public class ReMatchUseCase
{


    internal SessionDTO ReMatch(int mainPlayerId, int opponentPlayerId)
    {
        RematchService service = new RematchService();
        return service.Execute(mainPlayerId, opponentPlayerId);
    }
}