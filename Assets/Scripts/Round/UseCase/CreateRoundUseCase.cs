﻿using Assets.Scripts.DTO;

public class CreateRoundUseCase
{

    public RoundDTO CreateRound(SessionDTO sessionDTO)
    {
        CreateRoundService service = new CreateRoundService();
        return service.Execute(sessionDTO);
    }

}
