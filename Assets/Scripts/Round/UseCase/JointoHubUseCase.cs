﻿namespace Assets.Scripts.ModulePlayer.Presenter
{
    public class JointoHubUseCase
    {
        public bool Execute(string groupName, string playerId)
        {
            JointoHubService service = new JointoHubService();
            return service.Execute(groupName, playerId);
        }
    }
}