﻿
using Assets.Scripts.DTO;
using Assets.Scripts.Round.Service;
using System.Collections.Generic;

namespace Assets.Scripts.ModuleRound.UseCase
{
    public class CheckAnswersUseCase
    {

        public AnswersCheckedDTO CheckAnswers(int roundId, int SessionId, int PlayerId, List<string> AnswersInInputs, int idOpponent = 0)
        {
            SendAnswersService service = new SendAnswersService();
            return service.Execute(CreateAnswersDTO(SessionId, PlayerId, AnswersInInputs, idOpponent, roundId));
        }



        private RoundAnswersDTO CreateAnswersDTO(int sessionID, int playerID, List<string> _answers, int idOpponent, int roundId)
        {
            RoundAnswersDTO roundAnswers = new RoundAnswersDTO
            {
                Answers = _answers,
                RoundID = roundId,
                PlayerID = playerID,
                SessionID = sessionID,
                IdOpponent = idOpponent
            };
            return roundAnswers;
        }


    }
}