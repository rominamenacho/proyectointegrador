﻿using Assets.Scripts.Round.Service;

namespace Assets.Scripts.ModulePlayer.Presenter
{
    internal class ObtainTimeUseCase
    {

        internal float ObtainTime(int idRound, int idSession, int idPlayer)
        {
            ObtainTimeService service = new ObtainTimeService();
            return service.Execute(idRound, idSession, idPlayer);
        }
    }
}