﻿using Assets.Scripts.DTO;
using Assets.Scripts.Round.Service;

public class DoMatchUseCase
{

    public SessionDTO Match(int playerId)
    {
        MatchService service = new MatchService();
        return service.Execute(playerId);
    }

}

