﻿using Assets.Scripts.DTO;
using Assets.Scripts.ModuleRound.Views;
using Assets.Scripts.Round.View;
using System.Collections.Generic;

public interface ISceneStartRoundView : IButtonCheckView, IButtonGoToMainMenu, IOkButtonPanelErrors
{

    public float Seconds { get; }
    public void ShowCorrectIconInScreen();
    public void ShowWrongIconInScreen();

    public void StartTimer(float seconds);
    public void StopTimer();
    public void DrawLetter(string letter);
    public void DrawScoreRound(string score);
    public void DrawCategories(List<string> categoriesName);
    public void FillIconsAnswers(AnswersCheckedDTO answersChecked);
    public void BlockInputs();
    public void DrawRoundCount(string v);
    void ShowMessageCreateSession();
    void ShowMessageStartRematch();
    void ShowMessageCreateNewRound();
}