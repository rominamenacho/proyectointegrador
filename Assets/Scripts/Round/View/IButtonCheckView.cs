﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.ModuleRound.Views
{
    public interface IButtonCheckView
    {
        event Action ButtonCheckGameClicked;

        event Action<List<string>> ButtonCheckGameClickedLoadAnswers;
        event Action ButtonCheckGameClickedCheckAnswers;

        public void FindAnswersInTextInputs();
        public void UpdateInteractablePropertyButtons();
        public void CreateErrorPanel(string message);
    }
}
