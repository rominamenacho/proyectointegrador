﻿using System;

namespace Assets.Scripts.ModuleRound.Views
{
    public interface IButtonGoToMainMenu
    {
        event Action ButtonGoToMainMenu;
        public void GoToMainMenu();
    }
}
