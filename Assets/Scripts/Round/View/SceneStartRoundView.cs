using Assets.Scripts.DTO;
using Assets.Scripts.ModulePlayer.Presenter;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneStartRoundView : MonoBehaviour, ISceneStartRoundView
{
    [SerializeField] private Button _checkButton;
    [SerializeField] private Button _backButton;
    [SerializeField] private Button _errorButton;
    [SerializeField] private GameObject _correctIcons;
    [SerializeField] private GameObject _wrongIcons;
    [SerializeField] private GameObject _inputField;
    [SerializeField] private GameObject _labels;
    [SerializeField] private GameObject _errorPanel;
    [SerializeField] private GameObject _errorBody;
    [SerializeField] private Text _letter;
    [SerializeField] private Text _score;
    [SerializeField] private Text _currentRound;
    [SerializeField] private Text _timer;
    [SerializeField] private Animator transition;
    [SerializeField] private TextMeshProUGUI transitionMessage;


    private float _seconds;
    private bool shouldDecrement = true;
    public float Seconds { get => _seconds; }

    private List<string> _answersInInputs = new List<string>();
    private List<bool> _showWrongOrCorrectIcon = new List<bool>();



    public event Action ButtonCheckGameClickedCheckAnswers = () => { };
    public event Action ButtonCheckGameClicked = () => { };
    public event Action ButtonGoToMainMenu = () => { };
    public event Action ButtonErrorClicked = () => { };
    public event Action<List<string>> ButtonCheckGameClickedLoadAnswers = (List<string> AnswersInInputs) => { };



    void Start()
    {
        SceneStartRoundPresenter _sceneStartRoundPresenter = new SceneStartRoundPresenter(this);
        AddListeners();
    }

    void Update()
    {
        Counter();
    }


    public void DrawLetter(string letter)
    {
        _letter.text = letter;
    }
    public void DrawScoreRound(string score)
    {
        _score.text = score;
    }
    public void BlockInputs()
    {
        foreach (Transform child in _inputField.transform)
        {
            child.gameObject.GetComponent<TMP_InputField>().interactable = false;
        }

    }
    public void FindAnswersInTextInputs()
    {
        _answersInInputs.Clear();
        foreach (Transform child in _inputField.transform)
        {
            if (child.gameObject.GetComponent<TMP_InputField>().text.ToUpper() != "")
            {
                _answersInInputs.Add(child.gameObject.GetComponent<TMP_InputField>().text.Trim().ToUpper());
            }
            else
            {
                _answersInInputs.Add("-");
            }
        }
    }

    public void ShowCorrectIconInScreen()
    {
        GameObject IconsName = GameObject.Find("CorrectIcons");
        int i = 0;
        foreach (Transform child in IconsName.transform)
        {
            if (_showWrongOrCorrectIcon[i])
            {
                child.gameObject.SetActive(true);
            }
            i++;
        }
    }
    public void ShowWrongIconInScreen()
    {
        GameObject IconsName = GameObject.Find("WrongIcons");
        int i = 0;
        foreach (Transform child in IconsName.transform)
        {
            if (!_showWrongOrCorrectIcon[i])
            {
                child.gameObject.SetActive(true);
            }
            i++;
        }
    }

    public void DrawCategories(List<string> categoriesName)
    {
        int position = 0;

        foreach (Transform child in _labels.transform)
        {
            child.gameObject.GetComponent<TextMeshProUGUI>().text = categoriesName[position];
            position++;
        }
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void UpdateInteractablePropertyButtons()
    {
        _checkButton.interactable = false;
        _backButton.interactable = true;
    }

    public void FillIconsAnswers(AnswersCheckedDTO answersChecked)
    {
        foreach (var answers in answersChecked.Answers)
        {
            _showWrongOrCorrectIcon.Add(answers.Correct);
        }
    }

    public void DrawRoundCount(string countOfRound)
    {
        _currentRound.text = countOfRound;
    }

    public void StartTimer(float seconds)
    {
        _seconds = seconds;
    }

    public void StopTimer()
    {
        shouldDecrement = false;
    }

    public void ShowErrorPanel(string error)
    {
        UpdateInteractablePropertyButtons();
        CreateErrorPanel(error);
    }

    public void CreateErrorPanel(string error)
    {
        _errorPanel.SetActive(true);
        _errorBody.GetComponent<TextMeshProUGUI>().text = error;
    }

    public void CloseErrorPanel()
    {
        _errorPanel.SetActive(false);

    }
    private void AddListeners()
    {
        _checkButton.onClick.AddListener(() =>
        {
            ButtonCheckGameClicked.Invoke();
            ButtonCheckGameClickedLoadAnswers.Invoke(_answersInInputs);
            ButtonCheckGameClickedCheckAnswers.Invoke();

        });
        _backButton.onClick.AddListener(() =>
        {
            ButtonGoToMainMenu.Invoke();
        });
        _errorButton.onClick.AddListener(() =>
        {
            ButtonErrorClicked.Invoke();
        });


    }
    private void Counter()
    {
        if (shouldDecrement)
        {
            if (_seconds <= 0)
            {
                BlockInputs();
            }
            else
            {
                _seconds -= Time.deltaTime;
                _timer.text = _seconds.ToString("f0");
            }

        }
    }

    public void ShowMessageCreateSession()
    {
        transitionMessage.text = "Creando nueva partida...";
    }


    public void ShowMessageStartRematch()
    {
        transitionMessage.text = "Comenzando rematch...";

    }

    public void ShowMessageCreateNewRound()
    {
        transitionMessage.text = "Comenzando nueva ronda...";
    }
}
