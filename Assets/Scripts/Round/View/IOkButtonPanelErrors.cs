﻿using System;

namespace Assets.Scripts.Round.View
{
    public interface IOkButtonPanelErrors
    {
        public event Action ButtonErrorClicked;
        public void ShowErrorPanel(string error);
        public void CloseErrorPanel();
    }
}
