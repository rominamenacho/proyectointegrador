﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Round.Service
{
    public class SaveTimeLeftService : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public bool Execute(int playerId, int idRound, int idSession, float seconds)
        {
            return Task.Run(() => SaveTime(playerId, idRound, idSession, seconds)).GetAwaiter().GetResult();
        }


        private async Task<bool> SaveTime(int playerId, int idRound, int idSession, float seconds)
        {
            if (IsValid(playerId, idRound, idSession))
            {
                var json = new Dictionary<string, object>();
                json.Add("playerId", playerId);
                json.Add("idRound", idRound);
                json.Add("idSession", idSession);
                json.Add("seconds", seconds);
                var jconverted = JsonConvert.SerializeObject(json);

                var queryString = new StringContent(jconverted.ToString(), Encoding.UTF8, "application/json");

                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.PostAsync(new Uri(ConnectionAPI.URL + "/api/SaveTimeLeftRound/"), queryString);
                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode) return true;
            }
            return false;
        }

        private bool IsValid(int playerId, int idRound, int idSession)
        {
            return (playerId > 0 && idRound > 0 && idSession > 0);
        }
    }
}
