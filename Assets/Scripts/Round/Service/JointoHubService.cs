﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.ModulePlayer.Presenter
{
    public class JointoHubService
    {
        public bool Execute(string groupName, string playerId)
        {
            if (AreInValidParameters(groupName, playerId)) return false;
            return Task.Run(() => JoinGame(groupName, playerId)).GetAwaiter().GetResult();
        }

        private bool AreInValidParameters(string groupName, string playerId)
        {
            return !Regex.Match(groupName, "[0-9]").Success
                   || String.IsNullOrEmpty(groupName)
                   || !Regex.Match(playerId, "[0-9]").Success
                   || String.IsNullOrEmpty(playerId);
        }

        private async Task<bool> JoinGame(string groupName, string playerId)
        {

            ConnectionHUB conHub = ConnectionHUB.GetInstance();
            try
            {
                if (ConnectionHUB.connection.State == 0)
                {
                    conHub.Connect();
                }
                await ConnectionHUB.connection.InvokeAsync("JoinGroup", groupName, playerId);
                return true;
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
                return false;
            }

        }
    }
}