﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Round.Service
{
    public class ObtainTimeService : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public float Execute(int idRound, int idSession, int idPlayer)
        {
            return Task.Run(() => ObtainTime(idRound, idSession, idPlayer)).GetAwaiter().GetResult();
        }


        private async Task<float> ObtainTime(int idRound, int idSession, int idPlayer)
        {
            float time = 0;

            if (!Validate(idRound, idSession, idPlayer)) return time;
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(string.Format(ConnectionAPI.URL + $"/api/RoundTimer/IdRound={idRound}&IdSession={idSession}&IdPlayer={idPlayer}"));

                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                time = response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<float>(responseBody) : 0;

            }
            catch (HttpRequestException httpRequestException)
            {
                Debug.Log(httpRequestException);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
            return time;
        }

        private bool Validate(int idRound, int idSession, int idPlayer)
        {
            return (idRound > 0 && idSession > 0 && idPlayer > 0);
        }
    }
}
