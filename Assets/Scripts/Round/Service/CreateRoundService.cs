﻿using Assets.Scripts;
using Assets.Scripts.DTO;
using Newtonsoft.Json;
using System;

using System.Net.Http;
using System.Threading.Tasks;

public class CreateRoundService : IDisposable
{
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }

    public RoundDTO Execute(SessionDTO sessionDTO)
    {
        return Task.Run(() =>
        CreateNewRoundAPI(sessionDTO)).GetAwaiter().GetResult();
    }
    private async Task<RoundDTO> CreateNewRoundAPI(SessionDTO _sessionDTO)
    {
        RoundDTO dto = new RoundDTO();

        if (!ValidateDto(_sessionDTO))
        {
            dto.ErrorDetails = CreateResponse(400, "Sesión no válida");
            return dto;
        }

        try
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(_sessionDTO).ToString(),
         System.Text.Encoding.UTF8, "application/json");
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(new Uri(ConnectionAPI.URL + "/api/startRound/"), stringContent);

            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            dto = JsonConvert.DeserializeObject<RoundDTO>(responseBody);
            if (response.IsSuccessStatusCode) dto.ErrorDetails = CreateResponse(200, "Creación Exitosa");
        }
        catch (HttpRequestException httpRequestException)
        {
            dto.ErrorDetails = CreateResponse(400, httpRequestException.Message);
        }
        catch (Exception e)
        {
            dto.ErrorDetails = CreateResponse(404, e.Message);
        }
        return dto;

    }
    private bool ValidateDto(SessionDTO _sessionDTO)
    {
        return (_sessionDTO.Id > 0);
    }
    private ErrorDetailsDTO CreateResponse(int statusCode, String message)
    {
        ErrorDetailsDTO errorDetail = new ErrorDetailsDTO();
        errorDetail.StatusCode = statusCode;
        errorDetail.Message = $"Detalles del error: { message}";
        return errorDetail;
    }

}