﻿using Assets.Scripts.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Round.Service
{
    public class MatchService
    {

        public SessionDTO Execute(int playerId)
        {
            return Task.Run(() => MatchAPI(playerId)).GetAwaiter().GetResult();
        }

        private async Task<SessionDTO> MatchAPI(int playerId)
        {
            SessionDTO sessionDTO = new SessionDTO();
            if (!IsValid(playerId))
            {
                CreateError(sessionDTO, 400, "El id del jugador es inválido");
                return sessionDTO;
            }
            else
            {
                var json = new Dictionary<string, object>();
                json.Add("Id", playerId);

                var jconverted = JsonConvert.SerializeObject(json);

                var queryString = new StringContent(jconverted.ToString(), Encoding.UTF8, "application/json");
                try
                {

                    HttpClient client = new HttpClient();
                    HttpResponseMessage response = await client.PostAsync(new Uri(ConnectionAPI.URL + "/api/match/"), queryString);

                    response.EnsureSuccessStatusCode();

                    string responseBody = await response.Content.ReadAsStringAsync();

                    sessionDTO = JsonConvert.DeserializeObject<SessionDTO>(responseBody);
                }
                catch (HttpRequestException ex)
                {
                    CreateError(sessionDTO, 400, ex.Message);
                }
            }

            return sessionDTO;

        }

        private static void CreateError(SessionDTO sessionDTO, int statusCode, string message)
        {
            sessionDTO.ErrorDetails.StatusCode = statusCode;
            sessionDTO.ErrorDetails.Message = message;
        }

        private static bool IsValid(int playerId)
        {
            return playerId > 0;
        }
    }
}
