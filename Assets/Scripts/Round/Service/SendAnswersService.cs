﻿using Assets.Scripts.DTO;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Round.Service
{
    public class SendAnswersService : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public AnswersCheckedDTO Execute(RoundAnswersDTO dto)
        {
            return Task.Run(() => SendAnswers(dto)).GetAwaiter().GetResult();
        }

        private async Task<AnswersCheckedDTO> SendAnswers(RoundAnswersDTO dto)
        {
            AnswersCheckedDTO answersCheckedDTO = new AnswersCheckedDTO();

            if (!ValidateDto(dto))
            {
                answersCheckedDTO.ErrorDetailsDTO.Add(CreateError(400, "Alguno de los parametros no fue válido"));
                return answersCheckedDTO;
            }
            var stringContent = new StringContent(JsonConvert.SerializeObject(dto).ToString(), Encoding.UTF8, "application/json");
            HttpClient client = new HttpClient();
            try
            {
                HttpResponseMessage response = await client.PostAsync(new Uri(ConnectionAPI.URL + "/api/sendAnswers/"), stringContent);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                answersCheckedDTO = JsonConvert.DeserializeObject<AnswersCheckedDTO>(responseBody);
            }
            catch (HttpRequestException httpRequestException)
            {
                answersCheckedDTO.ErrorDetailsDTO.Add(CreateError(400, httpRequestException.Message));
            }
            catch (Exception e)
            {
                answersCheckedDTO.ErrorDetailsDTO.Add(CreateError(404, e.Message));
            }
            return answersCheckedDTO;
        }

        private ErrorDetailsDTO CreateError(int statusCode, String message)
        {
            ErrorDetailsDTO errorDetail = new ErrorDetailsDTO();
            errorDetail.StatusCode = statusCode;
            errorDetail.Message = $"Detalles del error: { message}";
            return errorDetail;
        }
        private bool ValidateDto(RoundAnswersDTO dto)
        {
            return (dto.SessionID > 0) &&
                (dto.PlayerID > 0) &&
                (dto.RoundID > 0);


        }
    }
}
