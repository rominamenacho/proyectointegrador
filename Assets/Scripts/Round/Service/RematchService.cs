﻿using Assets.Scripts.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Round.Service
{
    public class RematchService : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public SessionDTO Execute(int mainPlayerId, int opponentPlayerId)
        {
            return Task.Run(() =>
            ReMatchAPI(mainPlayerId, opponentPlayerId)).GetAwaiter().GetResult();
        }

        private async Task<SessionDTO> ReMatchAPI(int mainPlayerId, int opponentPlayerId)
        {
            SessionDTO sessionDTO = new SessionDTO();

            if (!AreValid(mainPlayerId, opponentPlayerId))
            {
                CreateError(sessionDTO, 400, "Alguno de los id son inválidos");
                return sessionDTO;
            }
            else
            {
                var json = new Dictionary<string, object>();
                json.Add("IdMainPlayer", mainPlayerId);
                json.Add("IdOpponent", opponentPlayerId);

                var jconverted = JsonConvert.SerializeObject(json);

                var queryString = new StringContent(jconverted.ToString(), Encoding.UTF8, "application/json");
                try
                {
                    HttpClient client = new HttpClient();
                    HttpResponseMessage response = await client.PostAsync(new Uri(ConnectionAPI.URL + "/api/ReMatch/"), queryString);

                    response.EnsureSuccessStatusCode();

                    string responseBody = await response.Content.ReadAsStringAsync();

                    sessionDTO = JsonConvert.DeserializeObject<SessionDTO>(responseBody);

                    if (sessionDTO.ErrorDetails.StatusCode == 200) CreateError(sessionDTO, 200, "Rematch exitoso");
                }
                catch (HttpRequestException ex)
                {
                    CreateError(sessionDTO, 400, ex.Message);
                }

                return sessionDTO;
            }
        }

        private static bool AreValid(int mainPlayerId, int opponentPlayerId)
        {
            return mainPlayerId > 0 && opponentPlayerId > 0;
        }
        private static void CreateError(SessionDTO sessionDTO, int statusCode, string message)
        {
            sessionDTO.ErrorDetails.StatusCode = statusCode;
            sessionDTO.ErrorDetails.Message = message;
        }
    }

}