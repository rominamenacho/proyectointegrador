﻿using Assets.Scripts.DTO;
using Assets.Scripts.ModuleRound.UseCase;
using Assets.Scripts.ModuleSession.UseCase;
using Assets.Scripts.Round.UseCase;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace Assets.Scripts.ModulePlayer.Presenter
{
    public class SceneStartRoundPresenter
    {
        private AnswersCheckedDTO _answersChecked;
        private SessionDTO _sessionDTO;
        private ISceneStartRoundView _view;

        public SessionDTO SessionDTO { get => _sessionDTO; }
        public AnswersCheckedDTO AnswersChecked { get => _answersChecked; }

        public SceneStartRoundPresenter(ISceneStartRoundView view)
        {
            _view = view;
            _sessionDTO = new SessionDTO();
            InitSessionGame();
            SuscribeEvents();
            DrawScreen();
        }


        public virtual void InitSessionGame()
        {

            if (PlayerPrefs.GetInt("idSession") > 0)
            {
                CreateNewRound(PlayerPrefs.GetInt("idSession"));
                return;
            }
            if (PlayerPrefs.GetInt("idOpponent") > 0)
            {
                StartRematch(PlayerPrefs.GetInt("idOpponent"));
                return;
            }

            CreateSession();
        }

        private void CreateNewRound(int idSession)
        {
            if (UpdateSessionDTO(idSession))
            {
                CreateRoundUseCase _createRoundUseCase = new CreateRoundUseCase();
                //  SessionDTO.Rounds.Add( _createRoundUseCase.CreateRound(_sessionDTO)); porque no hacemos un add ?
                _createRoundUseCase.CreateRound(_sessionDTO);
                PlayerPrefs.SetInt("CountOfRounds", CountOfRounds());
                UpdateSessionDTO(idSession);///si hacemos add no hay que actualizar de nuevo creo
                _view.ShowMessageCreateNewRound();
            }

        }
        private void CreateSession()
        {

            DoMatchUseCase _matchUseCase = new DoMatchUseCase();
            _sessionDTO = _matchUseCase.Match(PlayerPrefs.GetInt("PlayerId"));
            if (_sessionDTO.ErrorDetails.StatusCode == 200)
            {
                PlayerPrefs.SetInt("CountOfRounds", CountOfRounds());
                AddToGroupSession(_sessionDTO.Id.ToString(), PlayerPrefs.GetInt("PlayerId").ToString());
                _view.ShowMessageCreateSession();
            }
            else
            {
                _view.ShowErrorPanel("Hubo un problema al crear una nueva partida." + _sessionDTO.ErrorDetails.Message);
            }
        }

        private void StartRematch(int IdOpponent)
        {
            ReMatchUseCase useCase = new ReMatchUseCase();
            _sessionDTO = useCase.ReMatch(PlayerPrefs.GetInt("PlayerId"), IdOpponent);
            AddToGroupSession(_sessionDTO.Id.ToString(), PlayerPrefs.GetInt("PlayerId").ToString());
            _view.ShowMessageStartRematch();

        }


        private void AddToGroupSession(string groupName, string playerId)
        {

            JointoHubUseCase usecase = new JointoHubUseCase();
            if (!usecase.Execute(groupName, playerId))
                _view.CreateErrorPanel("No se pudo establecer una conexion con el hub para recibir notificaciones");
        }

        private void BlockInputs()
        {
            _view.BlockInputs();
        }
        private void CloseErrorPanel()
        {
            _view.CloseErrorPanel();
            LoadSceneMainMenu();
            //   Debug.Log("REEVISAR CloseErrorPanel, EN CASO DEL JOIN HUB, NO ESTA BIEN QUE recargue PANTALLA");
        }

        private void CheckAnswers(List<string> AnswersInInputs)
        {
            CheckAnswersUseCase _checkAnswersUseCase = new CheckAnswersUseCase();
            _answersChecked = new AnswersCheckedDTO();

            (int, int) playersIds = GetPlayersId();

            _answersChecked = _checkAnswersUseCase.CheckAnswers(_sessionDTO.Rounds.LastOrDefault().Id,
                                                                _sessionDTO.Id,
                                                                playersIds.Item2,
                                                                AnswersInInputs,
                                                                playersIds.Item1);

            if (ThereAreErrorsOnAnswersChecked())
            {
                ShowErrorsCheckAnswers();
                return;
            }

            _view.DrawScoreRound(GetScore());
            ShowValidateIconsInScreen();
            UpdatePlayerPanel();
            SaveTimeLeft();

        }

        private void ShowErrorsCheckAnswers()
        {
            var errors = _answersChecked.ErrorDetailsDTO
                                        .Where(x => x.StatusCode != 200)
                                        .Select(x => x.Message)
                                        .Aggregate((i, j) => i + ". " + j)
                                        .ToString();
            _view.ShowErrorPanel(errors);
        }

        private bool ThereAreErrorsOnAnswersChecked()
        {
            return (_answersChecked.ErrorDetailsDTO.Count(x => x.StatusCode != 200) > 0);
        }

        private (int, int) GetPlayersId()
        {
            return (PlayerPrefs.GetInt("idOpponent"), PlayerPrefs.GetInt("PlayerId"));
        }

        private int CountOfRounds()
        {
            return _sessionDTO.Rounds.Count;
        }
        private void DrawLetter()
        {
            _view.DrawLetter(ObtainLetter());
        }
        private void DrawScreen()
        {
            if (_sessionDTO.ErrorDetails.StatusCode == 200)
            {
                DrawCategories();
                DrawLetter();
                DrawRoundCount();
                DrawTime();
            }
            else
            {
                _view.ShowErrorPanel("Error al dibujar la escena. " + _sessionDTO.ErrorDetails.Message);
            }
        }

        private void DrawTime()
        {
            _view.StartTimer(GetTime());
        }

        private void DrawCategories()
        {
            List<string> categoriesName = new List<string>();
            categoriesName = ObtainCategories();
            _view.DrawCategories(categoriesName);

        }
        private void DrawRoundCount()
        {
            _view.DrawRoundCount(CountOfRounds().ToString());
        }
        private void FindAnswersInTextInputs()
        {
            _view.FindAnswersInTextInputs();
        }
        private float GetTime()
        {
            ObtainTimeUseCase useCase = new ObtainTimeUseCase();
            return useCase.ObtainTime(_sessionDTO.Rounds.LastOrDefault().Id, _sessionDTO.Id, PlayerPrefs.GetInt("PlayerId"));
        }
        private string GetScore()
        {
            return _answersChecked.CorrectCount.ToString();
        }
        private void LoadSceneMainMenu()
        {
            _view.GoToMainMenu();
        }
        private string ObtainLetter()
        {
            return _sessionDTO.Rounds.LastOrDefault().Letter.ToString();
        }
        private List<string> ObtainCategories()
        {
            List<string> categories = new List<string>();
            foreach (var item in _sessionDTO.Rounds.LastOrDefault().Categories)
            {
                categories.Add(item.Name);
            }
            return categories;
        }
        private void SaveTimeLeft()
        {
            SaveTimeLeftUseCase useCase = new SaveTimeLeftUseCase();
            bool updateTimer = useCase.SaveTime(PlayerPrefs.GetInt("PlayerId"), _sessionDTO.Rounds.LastOrDefault().Id, _sessionDTO.Id, _view.Seconds);
            if (!updateTimer) _view.ShowErrorPanel("No fue posible guardar los segundos restantes");
        }
        private void ShowValidateIconsInScreen()
        {
            _view.FillIconsAnswers(_answersChecked);
            _view.ShowCorrectIconInScreen();
            _view.ShowWrongIconInScreen();
        }
        private void StopTimer()
        {
            _view.StopTimer();
        }
        private void SuscribeEvents()
        {
            _view.ButtonCheckGameClicked += FindAnswersInTextInputs;
            _view.ButtonCheckGameClicked += BlockInputs;

            _view.ButtonCheckGameClickedLoadAnswers += CheckAnswers;

            _view.ButtonCheckGameClickedCheckAnswers += StopTimer;

            _view.ButtonCheckGameClicked += UpdateInteractableButtons;
            _view.ButtonGoToMainMenu += LoadSceneMainMenu;
            _view.ButtonErrorClicked += CloseErrorPanel;

        }
        private bool UpdateSessionDTO(int idSession)
        {
            GetSessionUseCase updateSession = new GetSessionUseCase();

            _sessionDTO = updateSession.GetSessionById(idSession);
            if (_sessionDTO.ErrorDetails.StatusCode != 200)
            {
                _view.ShowErrorPanel($"No fue posible recuperar la partida. Intente más tarde. {_sessionDTO.ErrorDetails.Message}");
                return false;
            }
            return true;
        }

        private void UpdatePlayerPanel()
        {
            PlayerPrefs.SetInt("Victories", _answersChecked.PlayerVictories);
            PlayerPrefs.SetInt("Coins", _answersChecked.PlayerCoins);
        }
        private void UpdateInteractableButtons()
        {
            _view.UpdateInteractablePropertyButtons();
        }
        private void UnsubscribeEvents()
        {
            _view.ButtonCheckGameClicked -= FindAnswersInTextInputs;
            _view.ButtonCheckGameClicked -= BlockInputs;
            _view.ButtonCheckGameClickedLoadAnswers -= CheckAnswers;
            _view.ButtonCheckGameClickedCheckAnswers -= StopTimer;
            _view.ButtonCheckGameClicked -= UpdateInteractableButtons;
            _view.ButtonGoToMainMenu -= LoadSceneMainMenu;
            _view.ButtonErrorClicked -= CloseErrorPanel;

        }
    }
}
