﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class ConnectionHUB
    {

        private static ConnectionHUB instance;

        public static HubConnection connection;

        private ConnectionHUB()
        {
            GetConnectionHUB();
        }
        public static ConnectionHUB GetInstance()
        {
            if (instance == null)
            {
                instance = new ConnectionHUB();

            }
            return instance;
        }

        private void GetConnectionHUB()
        {
            connection = new HubConnectionBuilder()
          // .WithUrl("https://localhost:5001/gameHub")
          .WithUrl("http://topictwistergame.somee.com/gameHub")
          .AddMessagePackProtocol()
          .Build();

        }

        public async void Connect()
        {
            try
            {
                await connection.StartAsync();
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }







    }
}
