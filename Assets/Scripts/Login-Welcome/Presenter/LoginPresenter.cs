using Assets.Scripts.DTO;
using System;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEngine;

public class LoginPresenter : IDisposable
{
    ILoginView _loginView;



    public LoginPresenter(ILoginView LogInView)
    {
        _loginView = LogInView;
        SubscribeEvents();
    }

    private void SubscribeEvents()
    {
        _loginView.ButtonClicked += LoginToGame;
        _loginView.ButtonCloseClicked += CloseErrorComponent;
    }
    private void UnsubscribeEvents()
    {
        _loginView.ButtonClicked -= LoginToGame;
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
        UnsubscribeEvents();
    }

    ~LoginPresenter()
    {
        Dispose();
    }
    private void CloseErrorComponent()
    {
        _loginView.CloseErrorComponent();
    }
    public void LoginToGame(string name, string email)
    {
        if (!Validate(name, email))
        {
            _loginView.ShowErrorPanel("Datos invalidos");
            return;
        }

        PlayerDTO _playerDTO = CreatePlayer(name, email);
        if (_playerDTO.ErrorDetails.StatusCode != 200)
        {
            _loginView.ShowErrorPanel(_playerDTO.ErrorDetails.Message);
            return;
        }
        SaveDataPlayerPrefs(_playerDTO);
        _loginView.LoadMainMenuScene();

    }

    private PlayerDTO CreatePlayer(string name, string email)
    {
        CreatePlayerUseCase createUserUseCase = new CreatePlayerUseCase();
        PlayerDTO _playerDTO = createUserUseCase.Create(name, email);
        return _playerDTO;
    }

    public bool Validate(string name, string email)
    {
        return (IsValidEmail(email) && IsValidName(name));
    }

    private bool IsValidName(string name)
    {
        return Regex.Match(name, "[a-zA-Z]{4,25}").Success
        && !String.IsNullOrEmpty(name);
    }
    private bool IsValidEmail(string email)
    {
        if (string.IsNullOrWhiteSpace(email))
            return false;

        try
        {
            // Normalize the domain
            email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                  RegexOptions.None, TimeSpan.FromMilliseconds(200));

            // Examines the domain part of the email and normalizes it.
            string DomainMapper(Match match)
            {
                // Use IdnMapping class to convert Unicode domain names.
                var idn = new IdnMapping();

                // Pull out and process domain name (throws ArgumentException on invalid)
                string domainName = idn.GetAscii(match.Groups[2].Value);

                return match.Groups[1].Value + domainName;
            }
        }
        catch (RegexMatchTimeoutException e)
        {
            Debug.Log(e.Message);
            return false;
        }
        catch (ArgumentException e)
        {
            Debug.Log(e.Message);
            return false;
        }

        try
        {
            return Regex.IsMatch(email,
                @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
        catch (RegexMatchTimeoutException)
        {
            return false;
        }
    }


    private void SaveDataPlayerPrefs(PlayerDTO _playerDTO)
    {

        PlayerPrefs.SetString("UserCreated", "true");
        PlayerPrefs.SetInt("PlayerId", _playerDTO.Id);
        PlayerPrefs.SetString("Name", _playerDTO.Name);
        PlayerPrefs.SetInt("Coins", _playerDTO.Wallet.Coins);
        PlayerPrefs.SetInt("Victories", _playerDTO.Victories);
        PlayerPrefs.SetString("LoginDate", _playerDTO.LoginDate.ToString());
    }
}
