using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LogInView : MonoBehaviour, ILoginView
{
    [SerializeField] private Button _loginButton;
    [SerializeField] private Button _closeButtonErrorComponet;
    [SerializeField] private TMP_InputField _name;
    [SerializeField] private TMP_InputField _email;
    [SerializeField] private TMP_Text _errorLabel;
    [SerializeField] private GameObject _errorComponent;


    public event Action<string, string> ButtonClicked = (string _name, string _email) => { };
    public event Action ButtonCloseClicked = () => { };

    public void Start()
    {
        PlayerPrefs.DeleteAll();
        LoginPresenter _presenter = new LoginPresenter(this);
        AddListers();
    }
    public void ShowErrorPanel(string mensaje)
    {
        _errorComponent.SetActive(true);
        _errorLabel.text = mensaje;
    }
    public void CloseErrorComponent()
    {
        _errorComponent.SetActive(false);
    }
    public void LoadMainMenuScene()
    {
        SceneManager.LoadScene("MainMenu");
    }
    private void AddListers()
    {
        _loginButton.onClick.AddListener(() =>
        {
            ButtonClicked.Invoke(_name.text.Trim().ToString(), _email.text.Trim().ToString());
        });
        _closeButtonErrorComponet.onClick.AddListener(() =>
        {
            ButtonCloseClicked.Invoke();
        });
    }
}
