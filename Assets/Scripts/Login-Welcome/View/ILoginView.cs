using System;

public interface ILoginView
{
    event Action<string, string> ButtonClicked;

    event Action ButtonCloseClicked;
    public void LoadMainMenuScene();
    public void CloseErrorComponent();
    void ShowErrorPanel(string message);
}
