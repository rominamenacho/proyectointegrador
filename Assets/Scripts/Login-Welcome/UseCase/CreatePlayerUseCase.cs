using Assets.Scripts.DTO;
using Assets.Scripts.Login_Welcome.Service;

public class CreatePlayerUseCase
{


    public PlayerDTO Create(string name, string email)
    {
        CreatePlayerService service = new CreatePlayerService();
        return service.Execute(name, email);
    }


}
