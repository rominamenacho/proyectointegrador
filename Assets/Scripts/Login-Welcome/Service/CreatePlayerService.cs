﻿using Assets.Scripts.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Login_Welcome.Service
{
    class CreatePlayerService
    {


        internal PlayerDTO Execute(string name, string email)
        {
            return Task.Run(() => CreatePlayerAPI(name, email)).GetAwaiter().GetResult();
        }

        private async Task<PlayerDTO> CreatePlayerAPI(string name, string email)
        {
            PlayerDTO playerDTO = new PlayerDTO();
            var json = new Dictionary<string, object>();
            json.Add("Name", name);
            json.Add("Email", email);

            var jconverted = JsonConvert.SerializeObject(json);

            var queryString = new StringContent(jconverted.ToString(), Encoding.UTF8, "application/json");

            try
            {
                HttpClient cliente = new HttpClient();

                HttpResponseMessage response = await cliente.PostAsync(new Uri(ConnectionAPI.URL + "/api/players/add"), queryString);

                if (!response.IsSuccessStatusCode)
                {
                    playerDTO.ErrorDetails.StatusCode = 500;
                    return playerDTO;
                }
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                playerDTO = JsonConvert.DeserializeObject<PlayerDTO>(responseBody);

            }
            catch (System.Net.Sockets.SocketException socketException)
            {
                playerDTO.ErrorDetails.StatusCode = 500;
                playerDTO.ErrorDetails.Message = socketException.Message;
            }
            catch (Exception exception)
            {
                playerDTO.ErrorDetails.StatusCode = 500;
                playerDTO.ErrorDetails.Message = exception.Message;
            }

            return playerDTO;
        }


    }
}
